-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.19 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table foreverbonhommes.backend_access_log
CREATE TABLE IF NOT EXISTS `backend_access_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.backend_access_log: ~1 rows (approximately)
/*!40000 ALTER TABLE `backend_access_log` DISABLE KEYS */;
INSERT INTO `backend_access_log` (`id`, `user_id`, `ip_address`, `created_at`, `updated_at`) VALUES
	(1, 1, '127.0.0.1', '2018-06-28 00:10:57', '2018-06-28 00:10:57'),
	(2, 1, '127.0.0.1', '2018-06-28 18:19:47', '2018-06-28 18:19:47');
/*!40000 ALTER TABLE `backend_access_log` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.backend_users
CREATE TABLE IF NOT EXISTS `backend_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persist_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `is_activated` tinyint(1) NOT NULL DEFAULT '0',
  `role_id` int(10) unsigned DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_unique` (`login`),
  UNIQUE KEY `email_unique` (`email`),
  KEY `act_code_index` (`activation_code`),
  KEY `reset_code_index` (`reset_password_code`),
  KEY `admin_role_index` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.backend_users: ~1 rows (approximately)
/*!40000 ALTER TABLE `backend_users` DISABLE KEYS */;
INSERT INTO `backend_users` (`id`, `first_name`, `last_name`, `login`, `email`, `password`, `activation_code`, `persist_code`, `reset_password_code`, `permissions`, `is_activated`, `role_id`, `activated_at`, `last_login`, `created_at`, `updated_at`, `is_superuser`) VALUES
	(1, 'Admin', 'Person', 'admin', 'admin@domain.tld', '$2y$10$5SssnV0qEyqUAGQFr2GfxekYkDue0pXz0pIJYBqWN53mTK/8tHtXC', NULL, '$2y$10$xYhQYMT9Ofp6UCMiVzW2k.odLFtfsgqlEIY0rbRPxc3C3PfbodBIS', NULL, '', 1, 2, NULL, '2018-06-28 18:19:44', '2018-06-27 23:47:49', '2018-06-28 18:19:44', 1);
/*!40000 ALTER TABLE `backend_users` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.backend_users_groups
CREATE TABLE IF NOT EXISTS `backend_users_groups` (
  `user_id` int(10) unsigned NOT NULL,
  `user_group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`user_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.backend_users_groups: ~1 rows (approximately)
/*!40000 ALTER TABLE `backend_users_groups` DISABLE KEYS */;
INSERT INTO `backend_users_groups` (`user_id`, `user_group_id`) VALUES
	(1, 1);
/*!40000 ALTER TABLE `backend_users_groups` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.backend_user_groups
CREATE TABLE IF NOT EXISTS `backend_user_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_new_user_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_unique` (`name`),
  KEY `code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.backend_user_groups: ~1 rows (approximately)
/*!40000 ALTER TABLE `backend_user_groups` DISABLE KEYS */;
INSERT INTO `backend_user_groups` (`id`, `name`, `created_at`, `updated_at`, `code`, `description`, `is_new_user_default`) VALUES
	(1, 'Owners', '2018-06-27 23:47:49', '2018-06-27 23:47:49', 'owners', 'Default group for website owners.', 0);
/*!40000 ALTER TABLE `backend_user_groups` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.backend_user_preferences
CREATE TABLE IF NOT EXISTS `backend_user_preferences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `namespace` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `user_item_index` (`user_id`,`namespace`,`group`,`item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.backend_user_preferences: ~0 rows (approximately)
/*!40000 ALTER TABLE `backend_user_preferences` DISABLE KEYS */;
/*!40000 ALTER TABLE `backend_user_preferences` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.backend_user_roles
CREATE TABLE IF NOT EXISTS `backend_user_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `is_system` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_unique` (`name`),
  KEY `role_code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.backend_user_roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `backend_user_roles` DISABLE KEYS */;
INSERT INTO `backend_user_roles` (`id`, `name`, `code`, `description`, `permissions`, `is_system`, `created_at`, `updated_at`) VALUES
	(1, 'Publisher', 'publisher', 'Site editor with access to publishing tools.', '', 1, '2018-06-27 23:47:48', '2018-06-27 23:47:48'),
	(2, 'Developer', 'developer', 'Site administrator with access to developer tools.', '', 1, '2018-06-27 23:47:48', '2018-06-27 23:47:48');
/*!40000 ALTER TABLE `backend_user_roles` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.backend_user_throttle
CREATE TABLE IF NOT EXISTS `backend_user_throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `is_suspended` tinyint(1) NOT NULL DEFAULT '0',
  `suspended_at` timestamp NULL DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  `banned_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `backend_user_throttle_user_id_index` (`user_id`),
  KEY `backend_user_throttle_ip_address_index` (`ip_address`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.backend_user_throttle: ~1 rows (approximately)
/*!40000 ALTER TABLE `backend_user_throttle` DISABLE KEYS */;
INSERT INTO `backend_user_throttle` (`id`, `user_id`, `ip_address`, `attempts`, `last_attempt_at`, `is_suspended`, `suspended_at`, `is_banned`, `banned_at`) VALUES
	(1, 1, '127.0.0.1', 0, NULL, 0, NULL, 0, NULL);
/*!40000 ALTER TABLE `backend_user_throttle` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.cache
CREATE TABLE IF NOT EXISTS `cache` (
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL,
  UNIQUE KEY `cache_key_unique` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.cache: ~0 rows (approximately)
/*!40000 ALTER TABLE `cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.clake_userextended_comments
CREATE TABLE IF NOT EXISTS `clake_userextended_comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `author_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.clake_userextended_comments: ~0 rows (approximately)
/*!40000 ALTER TABLE `clake_userextended_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `clake_userextended_comments` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.clake_userextended_fields
CREATE TABLE IF NOT EXISTS `clake_userextended_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('UE_FORM_TEXT','UE_FORM_CHECKBOX','UE_FORM_COLOR','UE_FORM_DATE','UE_FORM_EMAIL','UE_FORM_FILE','UE_FORM_NUMBER','UE_FORM_PASSWORD','UE_FORM_RADIO','UE_FORM_RANGE','UE_FORM_TEL','UE_FORM_TIME','UE_FORM_URL','UE_FORM_SWITCH') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'UE_FORM_TEXT',
  `validation` json NOT NULL,
  `data` json NOT NULL,
  `flags` json NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `clake_userextended_fields_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.clake_userextended_fields: ~7 rows (approximately)
/*!40000 ALTER TABLE `clake_userextended_fields` DISABLE KEYS */;
INSERT INTO `clake_userextended_fields` (`id`, `name`, `code`, `description`, `type`, `validation`, `data`, `flags`, `sort_order`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 'Nickname', 'nickname', 'A users nickname. These are not unique.', 'UE_FORM_TEXT', '{"max": "13", "min": "3", "flags": ["required"], "regex": "", "content": "alpha_num", "additional": ""}', '{"class": "", "placeholder": "Nickname.."}', '{"enabled": true, "encrypt": false, "editable": true, "registerable": false}', 1, NULL, '2018-06-28 00:14:25', '2018-06-28 00:14:25'),
	(2, 'Suspended', 'core-suspended', '[Core Field] Stores whether or not a user is suspended.', 'UE_FORM_CHECKBOX', '{"max": "", "min": "", "flags": [""], "regex": "", "content": "", "additional": ""}', '{"core": true, "class": "", "placeholder": "Suspended.."}', '{"enabled": true, "encrypt": false, "editable": false, "registerable": false}', 2, NULL, '2018-06-28 00:14:35', '2018-06-28 00:14:35'),
	(3, 'Banned', 'core-banned', '[Core Field] Stores whether or not a user is banned.', 'UE_FORM_CHECKBOX', '{"max": "", "min": "", "flags": [""], "regex": "", "content": "", "additional": ""}', '{"core": true, "class": "", "placeholder": "Banned.."}', '{"enabled": true, "encrypt": false, "editable": false, "registerable": false}', 3, NULL, '2018-06-28 00:14:35', '2018-06-28 00:14:35'),
	(4, 'Temp Banned', 'core-temp-banned', '[Core Field] Stores whether or not a user is temp banned.', 'UE_FORM_CHECKBOX', '{"max": "", "min": "", "flags": [""], "regex": "", "content": "", "additional": ""}', '{"core": true, "class": "", "unban_date": {"date": "2018-06-28 00:14:35.977174", "timezone": "UTC", "timezone_type": 3}, "placeholder": "Temp Banned.."}', '{"enabled": true, "encrypt": false, "editable": false, "registerable": false}', 4, NULL, '2018-06-28 00:14:35', '2018-06-28 00:14:35'),
	(5, 'Privacy Setting who can comment', 'core-privacy-can-comment', '[Core Field] Stores a users privacy setting for who can leave them comments.', 'UE_FORM_NUMBER', '{"max": "", "min": "", "flags": [""], "regex": "", "content": "", "additional": ""}', '{"core": true, "class": "", "placeholder": "Octal Code.."}', '{"enabled": true, "encrypt": false, "editable": false, "registerable": false}', 5, NULL, '2018-06-28 00:14:35', '2018-06-28 00:14:35'),
	(6, 'Privacy Setting who can view a profile', 'core-privacy-view-profile', '[Core Field] Stores a users privacy setting for who can view their profile.', 'UE_FORM_NUMBER', '{"max": "", "min": "", "flags": [""], "regex": "", "content": "", "additional": ""}', '{"core": true, "class": "", "placeholder": "Octal Code.."}', '{"enabled": true, "encrypt": false, "editable": false, "registerable": false}', 6, NULL, '2018-06-28 00:14:36', '2018-06-28 00:14:36'),
	(7, 'Privacy Setting who can search them', 'core-privacy-can-search', '[Core Field] Stores a users privacy setting for who can find them in search.', 'UE_FORM_NUMBER', '{"max": "", "min": "", "flags": [""], "regex": "", "content": "", "additional": ""}', '{"core": true, "class": "", "placeholder": "Octal Code.."}', '{"enabled": true, "encrypt": false, "editable": false, "registerable": false}', 7, NULL, '2018-06-28 00:14:36', '2018-06-28 00:14:36');
/*!40000 ALTER TABLE `clake_userextended_fields` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.clake_userextended_friends
CREATE TABLE IF NOT EXISTS `clake_userextended_friends` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_that_sent_request` int(11) NOT NULL,
  `user_that_accepted_request` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `accepted` tinyint(1) NOT NULL,
  `relation` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.clake_userextended_friends: ~0 rows (approximately)
/*!40000 ALTER TABLE `clake_userextended_friends` DISABLE KEYS */;
/*!40000 ALTER TABLE `clake_userextended_friends` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.clake_userextended_integrated_users
CREATE TABLE IF NOT EXISTS `clake_userextended_integrated_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `integration_id` bigint(20) NOT NULL,
  `type` enum('UE_INTEGRATIONS_FACEBOOK','UE_INTEGRATIONS_DISQUS') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'UE_INTEGRATIONS_FACEBOOK',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.clake_userextended_integrated_users: ~0 rows (approximately)
/*!40000 ALTER TABLE `clake_userextended_integrated_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `clake_userextended_integrated_users` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.clake_userextended_modules
CREATE TABLE IF NOT EXISTS `clake_userextended_modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `author` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `updated` tinyint(1) NOT NULL DEFAULT '1',
  `flags` json NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `module_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `clake_userextended_modules_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.clake_userextended_modules: ~0 rows (approximately)
/*!40000 ALTER TABLE `clake_userextended_modules` DISABLE KEYS */;
/*!40000 ALTER TABLE `clake_userextended_modules` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.clake_userextended_roles
CREATE TABLE IF NOT EXISTS `clake_userextended_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.clake_userextended_roles: ~0 rows (approximately)
/*!40000 ALTER TABLE `clake_userextended_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `clake_userextended_roles` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.clake_userextended_routes
CREATE TABLE IF NOT EXISTS `clake_userextended_routes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` int(11) NOT NULL,
  `last_accessed_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cascade` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.clake_userextended_routes: ~0 rows (approximately)
/*!40000 ALTER TABLE `clake_userextended_routes` DISABLE KEYS */;
/*!40000 ALTER TABLE `clake_userextended_routes` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.clake_userextended_routes_pivot
CREATE TABLE IF NOT EXISTS `clake_userextended_routes_pivot` (
  `route_id` int(10) unsigned NOT NULL,
  `restriction_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`route_id`,`restriction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.clake_userextended_routes_pivot: ~0 rows (approximately)
/*!40000 ALTER TABLE `clake_userextended_routes_pivot` DISABLE KEYS */;
/*!40000 ALTER TABLE `clake_userextended_routes_pivot` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.clake_userextended_route_restriction
CREATE TABLE IF NOT EXISTS `clake_userextended_route_restriction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `attempts` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('UE_WHITELIST','UE_BLACKLIST') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'UE_WHITELIST',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.clake_userextended_route_restriction: ~0 rows (approximately)
/*!40000 ALTER TABLE `clake_userextended_route_restriction` DISABLE KEYS */;
/*!40000 ALTER TABLE `clake_userextended_route_restriction` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.clake_userextended_timezones
CREATE TABLE IF NOT EXISTS `clake_userextended_timezones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `abbr` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `utc` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `offset` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=198 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.clake_userextended_timezones: ~197 rows (approximately)
/*!40000 ALTER TABLE `clake_userextended_timezones` DISABLE KEYS */;
INSERT INTO `clake_userextended_timezones` (`id`, `abbr`, `name`, `utc`, `offset`, `count`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 'UTC', 'Coordinated Universal Time', 'UTC', '0', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(2, 'BIT', 'Baker Island Time', 'UTC-12', '-12', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(3, 'NUT', 'Niue Time', 'UTC-11', '-11', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(4, 'SST', 'Samoa Standard Time', 'UTC-11', '-11', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(5, 'CKT', 'Cook Island Time', 'UTC-10', '-10', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(6, 'HAST', 'Hawaii-Aleutian Standard Time', 'UTC-10', '-10', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(7, 'HST', 'Hawaii Standard Time', 'UTC-10', '-10', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(8, 'TAHT', 'Tahiti Time', 'UTC-10', '-10', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(9, 'MART', 'Marquesas Islands Time', 'UTC-09:30', '-09:30', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(10, 'MIT', 'Marquesas Islands Time', 'UTC-09:30', '-09:30', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(11, 'AKST', 'Alaska Standard Time', 'UTC-09', '-9', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(12, 'GAMT', 'Gambier Islands', 'UTC-09', '-9', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(13, 'GIT', 'Gambier Island Time', 'UTC-09', '-9', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(14, 'HADT', 'Hawaii-Aleutian Daylight Time', 'UTC-09', '-9', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(15, 'AKDT', 'Alaska Daylight Time', 'UTC-08', '-8', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(16, 'CIST', 'Clipperton Island Standard Time', 'UTC-08', '-8', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(17, 'PST', 'Pacific Standard Time (North America)', 'UTC-08', '-8', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(18, 'MST', 'Mountain Standard Time (North America)', 'UTC-07', '-7', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(19, 'PDT', 'Pacific Daylight Time (North America)', 'UTC-07', '-7', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(20, 'CST', 'Central Standard Time (North America)', 'UTC-06', '-6', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(21, 'EAST', 'Easter Island Standard Time', 'UTC-06', '-6', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(22, 'GALT', 'Galapagos Time', 'UTC-06', '-6', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(23, 'MDT', 'Mountain Daylight Time (North America)', 'UTC-06', '-6', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(24, 'CDT', 'Central Daylight Time (North America)', 'UTC-05', '-5', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(25, 'COT', 'Colombia Time', 'UTC-05', '-5', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(26, 'CST', 'Cuba Standard Time', 'UTC-05', '-5', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(27, 'EASST', 'Easter Island Standard Summer Time', 'UTC-05', '-5', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(28, 'ECT', 'Ecuador Time', 'UTC-05', '-5', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(29, 'EST', 'Eastern Standard Time (North America)', 'UTC-05', '-5', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(30, 'PET', 'Peru Time', 'UTC-05', '-5', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(31, 'VET', 'Venezuelan Standard Time', 'UTC-04:30', '-04:30', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(32, 'AMT', 'Amazon Time (Brazil)[2]', 'UTC-04', '-4', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(33, 'AST', 'Atlantic Standard Time', 'UTC-04', '-4', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(34, 'BOT', 'Bolivia Time', 'UTC-04', '-4', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(35, 'CDT', 'Cuba Daylight Time[3]', 'UTC-04', '-4', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(36, 'CLT', 'Chile Standard Time', 'UTC-04', '-4', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(37, 'COST', 'Colombia Summer Time', 'UTC-04', '-4', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(38, 'ECT', 'Eastern Caribbean Time (does not recognise DST)', 'UTC-04', '-4', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(39, 'EDT', 'Eastern Daylight Time (North America)', 'UTC-04', '-4', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(40, 'FKT', 'Falkland Islands Time', 'UTC-04', '-4', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(41, 'GYT', 'Guyana Time', 'UTC-04', '-4', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(42, 'PYT', 'Paraguay Time (Brazil)[7]', 'UTC-04', '-4', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(43, 'NST', 'Newfoundland Standard Time', 'UTC-03:30', '-03:30', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(44, 'NT', 'Newfoundland Time', 'UTC-03:30', '-03:30', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(45, 'ADT', 'Atlantic Daylight Time', 'UTC-03', '-3', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(46, 'AMST', 'Amazon Summer Time (Brazil)[1]', 'UTC-03', '-3', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(47, 'ART', 'Argentina Time', 'UTC-03', '-3', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(48, 'BRT', 'Brasilia Time', 'UTC-03', '-3', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(49, 'CLST', 'Chile Summer Time', 'UTC-03', '-3', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(50, 'FKST', 'Falkland Islands Standard Time', 'UTC-03', '-3', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(51, 'FKST', 'Falkland Islands Summer Time', 'UTC-03', '-3', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(52, 'GFT', 'French Guiana Time', 'UTC-03', '-3', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(53, 'PMST', 'Saint Pierre and Miquelon Standard Time', 'UTC-03', '-3', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(54, 'PYST', 'Paraguay Summer Time (Brazil)', 'UTC-03', '-3', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(55, 'ROTT', 'Rothera Research Station Time', 'UTC-03', '-3', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(56, 'SRT', 'Suriname Time', 'UTC-03', '-3', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(57, 'UYT', 'Uruguay Standard Time', 'UTC-03', '-3', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(58, 'NDT', 'Newfoundland Daylight Time', 'UTC-02:30', '-02:30', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(59, 'FNT', 'Fernando de Noronha Time', 'UTC-02', '-2', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(60, 'GST', 'South Georgia and the South Sandwich Islands', 'UTC-02', '-2', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(61, 'PMDT', 'Saint Pierre and Miquelon Daylight time', 'UTC-02', '-2', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(62, 'UYST', 'Uruguay Summer Time', 'UTC-02', '-2', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(63, 'AZOST', 'Azores Standard Time', 'UTC-01', '-1', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(64, 'CVT', 'Cape Verde Time', 'UTC-01', '-1', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(65, 'EGT', 'Eastern Greenland Time', 'UTC-01', '-1', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(66, 'GMT', 'Greenwich Mean Time', 'UTC', '0', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(67, 'UCT', 'Coordinated Universal Time', 'UTC', '0', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(68, 'WET', 'Western European Time', 'UTC', '0', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(69, 'Z', 'Zulu Time (Coordinated Universal Time)', 'UTC', '0', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(70, 'EGST', 'Eastern Greenland Summer Time', 'UTC+00', '0', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(71, 'BST', 'British Summer Time (British Standard Time from Feb 1968 to Oct 1971)', 'UTC+01', '1', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(72, 'CET', 'Central European Time', 'UTC+01', '1', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(73, 'DFT', 'AIX specific equivalent of Central European Time', 'UTC+01', '1', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(74, 'IST', 'Irish Standard Time', 'UTC+01', '1', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(75, 'MET', 'Middle European Time Same zone as CET', 'UTC+01', '1', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(76, 'WAT', 'West Africa Time', 'UTC+01', '1', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(77, 'WEDT', 'Western European Daylight Time', 'UTC+01', '1', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(78, 'WEST', 'Western European Summer Time', 'UTC+01', '1', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(79, 'CAT', 'Central Africa Time', 'UTC+02', '2', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(80, 'CEDT', 'Central European Daylight Time', 'UTC+02', '2', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(81, 'CEST', 'Central European Summer Time (Cf. HAEC)', 'UTC+02', '2', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(82, 'EET', 'Eastern European Time', 'UTC+02', '2', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(83, 'HAEC', 'Heure Avancée d\'Europe Centrale francised name for CEST', 'UTC+02', '2', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(84, 'IST', 'Israel Standard Time', 'UTC+02', '2', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(85, 'MEST', 'Middle European Saving Time Same zone as CEST', 'UTC+02', '2', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(86, 'SAST', 'South African Standard Time', 'UTC+02', '2', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(87, 'WAST', 'West Africa Summer Time', 'UTC+02', '2', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(88, 'AST', 'Arabia Standard Time', 'UTC+03', '3', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(89, 'EAT', 'East Africa Time', 'UTC+03', '3', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(90, 'EEDT', 'Eastern European Daylight Time', 'UTC+03', '3', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(91, 'EEST', 'Eastern European Summer Time', 'UTC+03', '3', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(92, 'FET', 'Further-eastern European Time', 'UTC+03', '3', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(93, 'IDT', 'Israel Daylight Time', 'UTC+03', '3', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(94, 'IOT', 'Indian Ocean Time', 'UTC+03', '3', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(95, 'SYOT', 'Showa Station Time', 'UTC+03', '3', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(96, 'IRST', 'Iran Standard Time', 'UTC+03:30', '+03:30', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(97, 'AMT', 'Armenia Time', 'UTC+04', '4', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(98, 'AZT', 'Azerbaijan Time', 'UTC+04', '4', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(99, 'GET', 'Georgia Standard Time', 'UTC+04', '4', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(100, 'GST', 'Gulf Standard Time', 'UTC+04', '4', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(101, 'MSK', 'Moscow Time', 'UTC+04', '4', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(102, 'MUT', 'Mauritius Time', 'UTC+04', '4', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(103, 'RET', 'Réunion Time', 'UTC+04', '4', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(104, 'SAMT', 'Samara Time', 'UTC+04', '4', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(105, 'SCT', 'Seychelles Time', 'UTC+04', '4', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(106, 'VOLT', 'Volgograd Time', 'UTC+04', '4', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(107, 'AFT', 'Afghanistan Time', 'UTC+04:30', '+04:30', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(108, 'AMST', 'Armenia Summer Time', 'UTC+05', '5', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(109, 'HMT', 'Heard and McDonald Islands Time', 'UTC+05', '5', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(110, 'MAWT', 'Mawson Station Time', 'UTC+05', '5', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(111, 'MVT', 'Maldives Time', 'UTC+05', '5', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(112, 'ORAT', 'Oral Time', 'UTC+05', '5', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(113, 'PKT', 'Pakistan Standard Time', 'UTC+05', '5', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(114, 'TFT', 'Indian/Kerguelen', 'UTC+05', '5', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(115, 'TJT', 'Tajikistan Time', 'UTC+05', '5', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(116, 'TMT', 'Turkmenistan Time', 'UTC+05', '5', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(117, 'UZT', 'Uzbekistan Time', 'UTC+05', '5', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(118, 'IST', 'Indian Standard Time', 'UTC+05:30', '+05:30', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(119, 'SLST', 'Sri Lanka Time', 'UTC+05:30', '+05:30', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(120, 'NPT', 'Nepal Time', 'UTC+05:45', '+05:45', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(121, 'BIOT', 'British Indian Ocean Time', 'UTC+06', '6', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(122, 'BST', 'Bangladesh Standard Time', 'UTC+06', '6', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(123, 'BTT', 'Bhutan Time', 'UTC+06', '6', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(124, 'KGT', 'Kyrgyzstan time', 'UTC+06', '6', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(125, 'VOST', 'Vostok Station Time', 'UTC+06', '6', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(126, 'YEKT', 'Yekaterinburg Time', 'UTC+06', '6', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(127, 'CCT', 'Cocos Islands Time', 'UTC+06:30', '+06:30', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(128, 'MMT', 'Myanmar Time', 'UTC+06:30', '+06:30', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(129, 'MST', 'Myanmar Standard Time', 'UTC+06:30', '+06:30', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(130, 'CXT', 'Christmas Island Time', 'UTC+07', '7', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(131, 'DAVT', 'Davis Time', 'UTC+07', '7', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(132, 'HOVT', 'Khovd Time', 'UTC+07', '7', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(133, 'ICT', 'Indochina Time', 'UTC+07', '7', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(134, 'KRAT', 'Krasnoyarsk Time', 'UTC+07', '7', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(135, 'OMST', 'Omsk Time', 'UTC+07', '7', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(136, 'THA', 'Thailand Standard Time', 'UTC+07', '7', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(137, 'ACT', 'ASEAN Common Time', 'UTC+08', '8', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(138, 'AWST', 'Australian Western Standard Time', 'UTC+08', '8', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(139, 'BDT', 'Brunei Time', 'UTC+08', '8', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(140, 'CHOT', 'Choibalsan', 'UTC+08', '8', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(141, 'CIT', 'Central Indonesia Time', 'UTC+08', '8', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(142, 'CST', 'China Standard Time', 'UTC+08', '8', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(143, 'CT', 'China time', 'UTC+08', '8', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(144, 'HKT', 'Hong Kong Time', 'UTC+08', '8', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(145, 'IRDT', 'Iran Daylight Time', 'UTC+08', '8', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(146, 'MST', 'Malaysia Standard Time', 'UTC+08', '8', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(147, 'MYT', 'Malaysia Time', 'UTC+08', '8', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(148, 'PHT', 'Philippine Time', 'UTC+08', '8', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(149, 'SGT', 'Singapore Time', 'UTC+08', '8', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(150, 'SST', 'Singapore Standard Time', 'UTC+08', '8', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(151, 'ULAT', 'Ulaanbaatar Time', 'UTC+08', '8', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(152, 'WST', 'Western Standard Time', 'UTC+08', '8', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(153, 'CWST', 'Central Western Standard Time (Australia)', 'UTC+08:45', '+08:45', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(154, 'AWDT', 'Australian Western Daylight Time', 'UTC+09', '9', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(155, 'EIT', 'Eastern Indonesian Time', 'UTC+09', '9', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(156, 'IRKT', 'Irkutsk Time', 'UTC+09', '9', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(157, 'JST', 'Japan Standard Time', 'UTC+09', '9', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(158, 'KST', 'Korea Standard Time', 'UTC+09', '9', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(159, 'TLT', 'Timor Leste Time', 'UTC+09', '9', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(160, 'ACST', 'Australian Central Standard Time', 'UTC+09:30', '+09:30', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(161, 'CST', 'Central Standard Time (Australia)', 'UTC+09:30', '+09:30', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(162, 'AEST', 'Australian Eastern Standard Time', 'UTC+10', '10', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(163, 'ChST', 'Chamorro Standard Time', 'UTC+10', '10', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(164, 'CHUT', 'Chuuk Time', 'UTC+10', '10', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(165, 'DDUT', 'Dumont d\'Urville Time', 'UTC+10', '10', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(166, 'EST', 'Eastern Standard Time (Australia)', 'UTC+10', '10', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(167, 'PGT', 'Papua New Guinea Time', 'UTC+10', '10', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(168, 'VLAT', 'Vladivostok Time', 'UTC+10', '10', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(169, 'YAKT', 'Yakutsk Time', 'UTC+10', '10', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(170, 'ACDT', 'Australian Central Daylight Time', 'UTC+10:30', '+10:30', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(171, 'CST', 'Central Summer Time (Australia)', 'UTC+10:30', '+10:30', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(172, 'LHST', 'Lord Howe Standard Time', 'UTC+10:30', '+10:30', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(173, 'AEDT', 'Australian Eastern Daylight Time', 'UTC+11', '11', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(174, 'KOST', 'Kosrae Time', 'UTC+11', '11', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(175, 'LHST', 'Lord Howe Summer Time', 'UTC+11', '11', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(176, 'MIST', 'Macquarie Island Station Time', 'UTC+11', '11', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(177, 'NCT', 'New Caledonia Time', 'UTC+11', '11', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(178, 'PONT', 'Pohnpei Standard Time', 'UTC+11', '11', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(179, 'SAKT', 'Sakhalin Island time', 'UTC+11', '11', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(180, 'SBT', 'Solomon Islands Time', 'UTC+11', '11', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(181, 'VUT', 'Vanuatu Time', 'UTC+11', '11', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(182, 'NFT', 'Norfolk Time', 'UTC+11:30', '+11:30', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(183, 'FJT', 'Fiji Time', 'UTC+12', '12', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(184, 'GILT', 'Gilbert Island Time', 'UTC+12', '12', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(185, 'MAGT', 'Magadan Time', 'UTC+12', '12', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(186, 'MHT', 'Marshall Islands', 'UTC+12', '12', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(187, 'NZST', 'New Zealand Standard Time', 'UTC+12', '12', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(188, 'PETT', 'Kamchatka Time', 'UTC+12', '12', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(189, 'TVT', 'Tuvalu Time', 'UTC+12', '12', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(190, 'WAKT', 'Wake Island Time', 'UTC+12', '12', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(191, 'CHAST', 'Chatham Standard Time', 'UTC+12:45', '+12:45', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(192, 'NZDT', 'New Zealand Daylight Time', 'UTC+13', '13', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(193, 'PHOT', 'Phoenix Island Time', 'UTC+13', '13', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(194, 'TOT', 'Tonga Time', 'UTC+13', '13', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(195, 'CHADT', 'Chatham Daylight Time', 'UTC+13:45', '+13:45', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(196, 'LINT', 'Line Islands Time', 'UTC+14', '14', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18'),
	(197, 'TKT', 'Tokelau Time', 'UTC+14', '14', 0, NULL, '2018-06-28 00:14:18', '2018-06-28 00:14:18');
/*!40000 ALTER TABLE `clake_userextended_timezones` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.cms_theme_data
CREATE TABLE IF NOT EXISTS `cms_theme_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_theme_data_theme_index` (`theme`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.cms_theme_data: ~1 rows (approximately)
/*!40000 ALTER TABLE `cms_theme_data` DISABLE KEYS */;
INSERT INTO `cms_theme_data` (`id`, `theme`, `data`, `created_at`, `updated_at`) VALUES
	(1, 'forever-bonhommes', '{"website_name":"Example","website_author":"Company Name","website_url":"https:\\/\\/example.org"}', '2018-06-28 00:16:59', '2018-06-28 00:16:59');
/*!40000 ALTER TABLE `cms_theme_data` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.cms_theme_logs
CREATE TABLE IF NOT EXISTS `cms_theme_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `old_content` longtext COLLATE utf8mb4_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_theme_logs_type_index` (`type`),
  KEY `cms_theme_logs_theme_index` (`theme`),
  KEY `cms_theme_logs_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.cms_theme_logs: ~0 rows (approximately)
/*!40000 ALTER TABLE `cms_theme_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_theme_logs` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.deferred_bindings
CREATE TABLE IF NOT EXISTS `deferred_bindings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `master_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `master_field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slave_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slave_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_bind` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `deferred_bindings_master_type_index` (`master_type`),
  KEY `deferred_bindings_master_field_index` (`master_field`),
  KEY `deferred_bindings_slave_type_index` (`slave_type`),
  KEY `deferred_bindings_slave_id_index` (`slave_id`),
  KEY `deferred_bindings_session_key_index` (`session_key`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.deferred_bindings: ~0 rows (approximately)
/*!40000 ALTER TABLE `deferred_bindings` DISABLE KEYS */;
/*!40000 ALTER TABLE `deferred_bindings` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci,
  `failed_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.failed_jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.jobs
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_reserved_at_index` (`queue`,`reserved_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.martin_forms_records
CREATE TABLE IF NOT EXISTS `martin_forms_records` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '(Empty)',
  `form_data` text COLLATE utf8mb4_unicode_ci,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unread` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.martin_forms_records: ~0 rows (approximately)
/*!40000 ALTER TABLE `martin_forms_records` DISABLE KEYS */;
INSERT INTO `martin_forms_records` (`id`, `group`, `form_data`, `ip`, `unread`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 'Wedding Guests', '{"first_name":null,"last_name":null,"accepts":null,"declines":null,"#_of_person(s)_attending":null}', '127.0.0.1', 0, '2018-06-28 20:42:22', '2018-06-28 20:41:38', '2018-06-28 20:42:22'),
	(2, 'Wedding Guests', '{"first_name":"Stanley","last_name":"Bonhomme","accepts":null,"declines":null,"#_of_person(s)_attending":"1"}', '127.0.0.1', 0, '2018-06-28 21:01:13', '2018-06-28 21:00:35', '2018-06-28 21:01:13'),
	(3, 'Wedding Guests', '{"first_name":"Stanley","last_name":"Bonhomme","accepts":null,"declines":null,"#_of_person(s)_attending":"2"}', '127.0.0.1', 0, '2018-06-28 21:04:35', '2018-06-28 21:04:16', '2018-06-28 21:04:35'),
	(4, '(Empty)', '{"first_name":"Stanley","last_name":"Bonhomme","accepts":null,"declines":null,"#_of_person(s)_attending":"1"}', '127.0.0.1', 0, '2018-06-28 21:08:17', '2018-06-28 21:05:11', '2018-06-28 21:08:17'),
	(5, 'Guest List', '{"first_name":"Stanley","last_name":"Bonhomme","accepts":null,"declines":null,"#_of_person(s)_attending":"2"}', '127.0.0.1', 0, '2018-06-28 21:11:44', '2018-06-28 21:11:26', '2018-06-28 21:11:44'),
	(6, 'Wedding Guest List', '{"first_name":"Stanley","last_name":"Bonhomme","guest_response":"ACCEPTS","#_of_person(s)_attending":"1"}', '127.0.0.1', 0, NULL, '2018-06-28 21:15:55', '2018-06-28 21:16:03');
/*!40000 ALTER TABLE `martin_forms_records` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.migrations: ~37 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2013_10_01_000001_Db_Deferred_Bindings', 1),
	(2, '2013_10_01_000002_Db_System_Files', 1),
	(3, '2013_10_01_000003_Db_System_Plugin_Versions', 1),
	(4, '2013_10_01_000004_Db_System_Plugin_History', 1),
	(5, '2013_10_01_000005_Db_System_Settings', 1),
	(6, '2013_10_01_000006_Db_System_Parameters', 1),
	(7, '2013_10_01_000007_Db_System_Add_Disabled_Flag', 1),
	(8, '2013_10_01_000008_Db_System_Mail_Templates', 1),
	(9, '2013_10_01_000009_Db_System_Mail_Layouts', 1),
	(10, '2014_10_01_000010_Db_Jobs', 1),
	(11, '2014_10_01_000011_Db_System_Event_Logs', 1),
	(12, '2014_10_01_000012_Db_System_Request_Logs', 1),
	(13, '2014_10_01_000013_Db_System_Sessions', 1),
	(14, '2015_10_01_000014_Db_System_Mail_Layout_Rename', 1),
	(15, '2015_10_01_000015_Db_System_Add_Frozen_Flag', 1),
	(16, '2015_10_01_000016_Db_Cache', 1),
	(17, '2015_10_01_000017_Db_System_Revisions', 1),
	(18, '2015_10_01_000018_Db_FailedJobs', 1),
	(19, '2016_10_01_000019_Db_System_Plugin_History_Detail_Text', 1),
	(20, '2016_10_01_000020_Db_System_Timestamp_Fix', 1),
	(21, '2017_08_04_121309_Db_Deferred_Bindings_Add_Index_Session', 1),
	(22, '2017_10_01_000021_Db_System_Sessions_Update', 1),
	(23, '2017_10_01_000022_Db_Jobs_FailedJobs_Update', 1),
	(24, '2017_10_01_000023_Db_System_Mail_Partials', 1),
	(25, '2013_10_01_000001_Db_Backend_Users', 2),
	(26, '2013_10_01_000002_Db_Backend_User_Groups', 2),
	(27, '2013_10_01_000003_Db_Backend_Users_Groups', 2),
	(28, '2013_10_01_000004_Db_Backend_User_Throttle', 2),
	(29, '2014_01_04_000005_Db_Backend_User_Preferences', 2),
	(30, '2014_10_01_000006_Db_Backend_Access_Log', 2),
	(31, '2014_10_01_000007_Db_Backend_Add_Description_Field', 2),
	(32, '2015_10_01_000008_Db_Backend_Add_Superuser_Flag', 2),
	(33, '2016_10_01_000009_Db_Backend_Timestamp_Fix', 2),
	(34, '2017_10_01_000010_Db_Backend_User_Roles', 2),
	(35, '2014_10_01_000001_Db_Cms_Theme_Data', 3),
	(36, '2016_10_01_000002_Db_Cms_Timestamp_Fix', 3),
	(37, '2017_10_01_000003_Db_Cms_Theme_Logs', 3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.rainlab_user_mail_blockers
CREATE TABLE IF NOT EXISTS `rainlab_user_mail_blockers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rainlab_user_mail_blockers_email_index` (`email`),
  KEY `rainlab_user_mail_blockers_template_index` (`template`),
  KEY `rainlab_user_mail_blockers_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.rainlab_user_mail_blockers: ~0 rows (approximately)
/*!40000 ALTER TABLE `rainlab_user_mail_blockers` DISABLE KEYS */;
/*!40000 ALTER TABLE `rainlab_user_mail_blockers` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.raviraj_rjgallery_categories
CREATE TABLE IF NOT EXISTS `raviraj_rjgallery_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `raviraj_rjgallery_categories_slug_index` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.raviraj_rjgallery_categories: ~0 rows (approximately)
/*!40000 ALTER TABLE `raviraj_rjgallery_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `raviraj_rjgallery_categories` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.raviraj_rjgallery_galleries
CREATE TABLE IF NOT EXISTS `raviraj_rjgallery_galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `published_at` timestamp NULL DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `raviraj_rjgallery_galleries_slug_index` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.raviraj_rjgallery_galleries: ~1 rows (approximately)
/*!40000 ALTER TABLE `raviraj_rjgallery_galleries` DISABLE KEYS */;
INSERT INTO `raviraj_rjgallery_galleries` (`id`, `name`, `created_at`, `updated_at`, `slug`, `description`, `published_at`, `published`) VALUES
	(1, 'engagement', '2018-06-28 00:17:56', '2018-06-28 00:18:06', 'engagement', '', '2018-06-01 00:18:04', 1);
/*!40000 ALTER TABLE `raviraj_rjgallery_galleries` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.raviraj_rjgallery_galleries_categories
CREATE TABLE IF NOT EXISTS `raviraj_rjgallery_galleries_categories` (
  `gallery_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`gallery_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.raviraj_rjgallery_galleries_categories: ~0 rows (approximately)
/*!40000 ALTER TABLE `raviraj_rjgallery_galleries_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `raviraj_rjgallery_galleries_categories` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.sessions
CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci,
  `last_activity` int(11) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.sessions: ~0 rows (approximately)
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.system_event_logs
CREATE TABLE IF NOT EXISTS `system_event_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `level` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `details` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `system_event_logs_level_index` (`level`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.system_event_logs: ~2 rows (approximately)
/*!40000 ALTER TABLE `system_event_logs` DISABLE KEYS */;
INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
	(1, 'error', 'Twig_Error_Syntax: Unknown "htmlcompress" tag in "C:\\laragon\\www\\foreverbonhommes/themes/forever-bonhommes/layouts/default.htm" at line 1. in C:\\laragon\\www\\foreverbonhommes\\vendor\\twig\\twig\\lib\\Twig\\Parser.php:158\nStack trace:\n#0 C:\\laragon\\www\\foreverbonhommes\\vendor\\twig\\twig\\lib\\Twig\\Parser.php(81): Twig_Parser->subparse(NULL, false)\n#1 C:\\laragon\\www\\foreverbonhommes\\vendor\\twig\\twig\\lib\\Twig\\Environment.php(533): Twig_Parser->parse(Object(Twig_TokenStream))\n#2 C:\\laragon\\www\\foreverbonhommes\\vendor\\twig\\twig\\lib\\Twig\\Environment.php(565): Twig_Environment->parse(Object(Twig_TokenStream))\n#3 C:\\laragon\\www\\foreverbonhommes\\vendor\\twig\\twig\\lib\\Twig\\Environment.php(368): Twig_Environment->compileSource(Object(Twig_Source))\n#4 C:\\laragon\\www\\foreverbonhommes\\modules\\cms\\classes\\Controller.php(374): Twig_Environment->loadTemplate(\'C:\\\\laragon\\\\www\\\\...\')\n#5 C:\\laragon\\www\\foreverbonhommes\\modules\\cms\\classes\\Controller.php(211): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#6 C:\\laragon\\www\\foreverbonhommes\\modules\\cms\\classes\\CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#7 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#8 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php(54): call_user_func_array(Array, Array)\n#9 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#10 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#11 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(169): Illuminate\\Routing\\Route->runController()\n#12 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Route->run()\n#13 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#14 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#15 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#16 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#17 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\View\\Middleware\\ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#18 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#19 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#20 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Session\\Middleware\\StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#21 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#22 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#23 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#24 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#25 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#26 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#28 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#29 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#30 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#31 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#32 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#33 C:\\laragon\\www\\foreverbonhommes\\vendor\\october\\rain\\src\\Router\\CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#34 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#35 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#36 C:\\laragon\\www\\foreverbonhommes\\plugins\\offline\\responsiveimages\\classes\\ResponsiveImagesMiddleware.php(28): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): OFFLINE\\ResponsiveImages\\Classes\\ResponsiveImagesMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#44 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#45 C:\\laragon\\www\\foreverbonhommes\\index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#46 {main}', NULL, '2018-06-28 00:16:59', '2018-06-28 00:16:59'),
	(2, 'error', 'October\\Rain\\Exception\\SystemException: Class name is not registered for the component "staticMenu". Check the component plugin. in C:\\laragon\\www\\foreverbonhommes\\modules\\cms\\classes\\ComponentManager.php:200\nStack trace:\n#0 C:\\laragon\\www\\foreverbonhommes\\modules\\cms\\classes\\Controller.php(911): Cms\\Classes\\ComponentManager->makeComponent(\'staticMenu\', Object(Cms5b3429e94f2a5832604462_966a4665e5f007f416c4ca2a36e38497Class), Array)\n#1 C:\\laragon\\www\\foreverbonhommes\\modules\\cms\\twig\\Extension.php(106): Cms\\Classes\\Controller->renderPartial(\'staticMenu\', Array, true)\n#2 C:\\laragon\\www\\foreverbonhommes\\storage\\cms\\twig\\66\\66abe18bd3bb2428c161b72d856728c7dcd99f3ce09213447ed63a546b821861.php(91): Cms\\Twig\\Extension->partialFunction(\'global/header\', Array, true)\n#3 C:\\laragon\\www\\foreverbonhommes\\vendor\\twig\\twig\\lib\\Twig\\Template.php(390): __TwigTemplate_a980121217a04ed1e8455f66ce7c8d33fa7cc935ac3d3082c58fddf14b0f0800->doDisplay(Array, Array)\n#4 C:\\laragon\\www\\foreverbonhommes\\vendor\\twig\\twig\\lib\\Twig\\Template.php(367): Twig_Template->displayWithErrorHandling(Array, Array)\n#5 C:\\laragon\\www\\foreverbonhommes\\vendor\\twig\\twig\\lib\\Twig\\Template.php(375): Twig_Template->display(Array)\n#6 C:\\laragon\\www\\foreverbonhommes\\modules\\cms\\classes\\Controller.php(375): Twig_Template->render(Array)\n#7 C:\\laragon\\www\\foreverbonhommes\\modules\\cms\\classes\\Controller.php(211): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#8 C:\\laragon\\www\\foreverbonhommes\\modules\\cms\\classes\\CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#9 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#10 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php(54): call_user_func_array(Array, Array)\n#11 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#12 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#13 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(169): Illuminate\\Routing\\Route->runController()\n#14 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Route->run()\n#15 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#16 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#17 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#18 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#19 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\View\\Middleware\\ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#20 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#21 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#22 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Session\\Middleware\\StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#23 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#24 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#25 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#26 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#27 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#28 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#29 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#30 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#31 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#32 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#33 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#34 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#35 C:\\laragon\\www\\foreverbonhommes\\vendor\\october\\rain\\src\\Router\\CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#36 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#37 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#38 C:\\laragon\\www\\foreverbonhommes\\plugins\\offline\\responsiveimages\\classes\\ResponsiveImagesMiddleware.php(28): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#39 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): OFFLINE\\ResponsiveImages\\Classes\\ResponsiveImagesMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#40 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#41 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#42 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#43 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#44 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#45 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#46 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#47 C:\\laragon\\www\\foreverbonhommes\\index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#48 {main}\n\nNext Twig_Error_Runtime: An exception has been thrown during the rendering of a template ("Class name is not registered for the component "staticMenu". Check the component plugin.") in "C:\\laragon\\www\\foreverbonhommes/themes/forever-bonhommes/layouts/default.htm" at line 34. in C:\\laragon\\www\\foreverbonhommes\\vendor\\twig\\twig\\lib\\Twig\\Template.php:405\nStack trace:\n#0 C:\\laragon\\www\\foreverbonhommes\\vendor\\twig\\twig\\lib\\Twig\\Template.php(367): Twig_Template->displayWithErrorHandling(Array, Array)\n#1 C:\\laragon\\www\\foreverbonhommes\\vendor\\twig\\twig\\lib\\Twig\\Template.php(375): Twig_Template->display(Array)\n#2 C:\\laragon\\www\\foreverbonhommes\\modules\\cms\\classes\\Controller.php(375): Twig_Template->render(Array)\n#3 C:\\laragon\\www\\foreverbonhommes\\modules\\cms\\classes\\Controller.php(211): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#4 C:\\laragon\\www\\foreverbonhommes\\modules\\cms\\classes\\CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#5 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#6 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php(54): call_user_func_array(Array, Array)\n#7 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#8 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#9 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(169): Illuminate\\Routing\\Route->runController()\n#10 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Route->run()\n#11 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#12 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#13 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#14 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#15 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\View\\Middleware\\ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#16 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#17 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#18 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Session\\Middleware\\StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#19 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#20 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#21 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#22 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#23 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#24 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#25 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#26 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#27 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#29 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#30 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#31 C:\\laragon\\www\\foreverbonhommes\\vendor\\october\\rain\\src\\Router\\CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#32 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#33 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#34 C:\\laragon\\www\\foreverbonhommes\\plugins\\offline\\responsiveimages\\classes\\ResponsiveImagesMiddleware.php(28): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#35 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): OFFLINE\\ResponsiveImages\\Classes\\ResponsiveImagesMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#36 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#37 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#38 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#42 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#43 C:\\laragon\\www\\foreverbonhommes\\index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#44 {main}', NULL, '2018-06-28 00:21:00', '2018-06-28 00:21:00'),
	(3, 'error', 'Cms\\Classes\\CmsException: The partial \'recaptcha\' is not found. in C:\\laragon\\www\\foreverbonhommes\\modules\\cms\\classes\\Controller.php:824\nStack trace:\n#0 C:\\laragon\\www\\foreverbonhommes\\modules\\cms\\twig\\Extension.php(106): Cms\\Classes\\Controller->renderPartial(\'::recaptcha\', Array, true)\n#1 C:\\laragon\\www\\foreverbonhommes\\storage\\cms\\twig\\b4\\b40d076ceb8160253be34873571b96a7bfc827c03ed7ce22c3be2158ddb22328.php(106): Cms\\Twig\\Extension->partialFunction(\'@recaptcha\', Array, true)\n#2 C:\\laragon\\www\\foreverbonhommes\\vendor\\twig\\twig\\lib\\Twig\\Template.php(390): __TwigTemplate_aee0a53dfc2914af5daa2bfa9549b95df05ecc3a51f16a8bcd46931b51886c85->doDisplay(Array, Array)\n#3 C:\\laragon\\www\\foreverbonhommes\\vendor\\twig\\twig\\lib\\Twig\\Template.php(367): Twig_Template->displayWithErrorHandling(Array, Array)\n#4 C:\\laragon\\www\\foreverbonhommes\\vendor\\twig\\twig\\lib\\Twig\\Template.php(375): Twig_Template->display(Array)\n#5 C:\\laragon\\www\\foreverbonhommes\\modules\\cms\\classes\\Controller.php(365): Twig_Template->render(Array)\n#6 C:\\laragon\\www\\foreverbonhommes\\modules\\cms\\classes\\Controller.php(211): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#7 C:\\laragon\\www\\foreverbonhommes\\modules\\cms\\classes\\CmsController.php(50): Cms\\Classes\\Controller->run(\'rsvp\')\n#8 [internal function]: Cms\\Classes\\CmsController->run(\'rsvp\')\n#9 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php(54): call_user_func_array(Array, Array)\n#10 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#11 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#12 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(169): Illuminate\\Routing\\Route->runController()\n#13 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Route->run()\n#14 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#15 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#16 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#17 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#18 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\View\\Middleware\\ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#19 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#20 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#21 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Session\\Middleware\\StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#22 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#23 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#24 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#25 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#26 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#27 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#32 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#33 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#34 C:\\laragon\\www\\foreverbonhommes\\vendor\\october\\rain\\src\\Router\\CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#35 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#36 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#37 C:\\laragon\\www\\foreverbonhommes\\plugins\\offline\\responsiveimages\\classes\\ResponsiveImagesMiddleware.php(28): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#38 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): OFFLINE\\ResponsiveImages\\Classes\\ResponsiveImagesMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#42 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#43 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#44 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#45 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#46 C:\\laragon\\www\\foreverbonhommes\\index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#47 {main}\n\nNext Twig_Error_Runtime: An exception has been thrown during the rendering of a template ("The partial \'recaptcha\' is not found.") in "C:\\laragon\\www\\foreverbonhommes/themes/forever-bonhommes/pages/rsvp.htm" at line 72. in C:\\laragon\\www\\foreverbonhommes\\vendor\\twig\\twig\\lib\\Twig\\Template.php:405\nStack trace:\n#0 C:\\laragon\\www\\foreverbonhommes\\vendor\\twig\\twig\\lib\\Twig\\Template.php(367): Twig_Template->displayWithErrorHandling(Array, Array)\n#1 C:\\laragon\\www\\foreverbonhommes\\vendor\\twig\\twig\\lib\\Twig\\Template.php(375): Twig_Template->display(Array)\n#2 C:\\laragon\\www\\foreverbonhommes\\modules\\cms\\classes\\Controller.php(365): Twig_Template->render(Array)\n#3 C:\\laragon\\www\\foreverbonhommes\\modules\\cms\\classes\\Controller.php(211): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#4 C:\\laragon\\www\\foreverbonhommes\\modules\\cms\\classes\\CmsController.php(50): Cms\\Classes\\Controller->run(\'rsvp\')\n#5 [internal function]: Cms\\Classes\\CmsController->run(\'rsvp\')\n#6 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php(54): call_user_func_array(Array, Array)\n#7 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#8 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#9 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(169): Illuminate\\Routing\\Route->runController()\n#10 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Route->run()\n#11 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#12 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#13 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#14 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#15 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\View\\Middleware\\ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#16 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#17 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#18 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Session\\Middleware\\StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#19 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#20 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#21 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#22 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#23 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#24 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#25 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#26 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#27 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#29 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#30 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#31 C:\\laragon\\www\\foreverbonhommes\\vendor\\october\\rain\\src\\Router\\CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#32 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#33 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#34 C:\\laragon\\www\\foreverbonhommes\\plugins\\offline\\responsiveimages\\classes\\ResponsiveImagesMiddleware.php(28): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#35 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): OFFLINE\\ResponsiveImages\\Classes\\ResponsiveImagesMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#36 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#37 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#38 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#42 C:\\laragon\\www\\foreverbonhommes\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#43 C:\\laragon\\www\\foreverbonhommes\\index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#44 {main}', NULL, '2018-06-28 20:04:36', '2018-06-28 20:04:36');
/*!40000 ALTER TABLE `system_event_logs` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.system_files
CREATE TABLE IF NOT EXISTS `system_files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `disk_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` int(11) NOT NULL,
  `content_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `system_files_field_index` (`field`),
  KEY `system_files_attachment_id_index` (`attachment_id`),
  KEY `system_files_attachment_type_index` (`attachment_type`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.system_files: ~14 rows (approximately)
/*!40000 ALTER TABLE `system_files` DISABLE KEYS */;
INSERT INTO `system_files` (`id`, `disk_name`, `file_name`, `file_size`, `content_type`, `title`, `description`, `field`, `attachment_id`, `attachment_type`, `is_public`, `sort_order`, `created_at`, `updated_at`) VALUES
	(1, '5b34297560bd0227578625.jpg', 'KISNA_STAN-1.jpg', 219529, 'image/jpeg', NULL, NULL, 'images', '1', 'Raviraj\\Rjgallery\\Models\\Gallery', 1, 1, '2018-06-28 00:19:01', '2018-06-28 00:22:28'),
	(2, '5b3429aef2bf0834040211.jpg', 'KISNA_STAN-2.jpg', 205983, 'image/jpeg', NULL, NULL, 'images', '1', 'Raviraj\\Rjgallery\\Models\\Gallery', 1, 2, '2018-06-28 00:19:59', '2018-06-28 00:22:29'),
	(3, '5b3429af4b70e643423939.jpg', 'KISNA_STAN-15.jpg', 556631, 'image/jpeg', NULL, NULL, 'images', '1', 'Raviraj\\Rjgallery\\Models\\Gallery', 1, 3, '2018-06-28 00:19:59', '2018-06-28 00:22:29'),
	(4, '5b3429afbbbde665845896.jpg', 'KISNA_STAN-19.jpg', 423152, 'image/jpeg', NULL, NULL, 'images', '1', 'Raviraj\\Rjgallery\\Models\\Gallery', 1, 5, '2018-06-28 00:19:59', '2018-06-28 00:22:29'),
	(5, '5b3429b079de8251332261.jpg', 'KISNA_STAN-44.jpg', 465182, 'image/jpeg', NULL, NULL, 'images', '1', 'Raviraj\\Rjgallery\\Models\\Gallery', 1, 7, '2018-06-28 00:20:00', '2018-06-28 00:22:29'),
	(6, '5b3429b0b8e8a016935547.jpg', 'KISNA_STAN-41.jpg', 300922, 'image/jpeg', NULL, NULL, 'images', '1', 'Raviraj\\Rjgallery\\Models\\Gallery', 1, 6, '2018-06-28 00:20:00', '2018-06-28 00:22:29'),
	(7, '5b3429b12cdfa300723684.jpg', 'KISNA_STAN-51.jpg', 247690, 'image/jpeg', NULL, NULL, 'images', '1', 'Raviraj\\Rjgallery\\Models\\Gallery', 1, 8, '2018-06-28 00:20:01', '2018-06-28 00:22:30'),
	(8, '5b3429b160a58207617977.jpg', 'KISNA_STAN-53.jpg', 147912, 'image/jpeg', NULL, NULL, 'images', '1', 'Raviraj\\Rjgallery\\Models\\Gallery', 1, 10, '2018-06-28 00:20:01', '2018-06-28 00:22:30'),
	(9, '5b3429b1d75fb251338499.jpg', 'KISNA_STAN-57.jpg', 112908, 'image/jpeg', NULL, NULL, 'images', '1', 'Raviraj\\Rjgallery\\Models\\Gallery', 1, 11, '2018-06-28 00:20:01', '2018-06-28 00:22:30'),
	(10, '5b3429b208c80907084491.jpg', 'KISNA_STAN-64.jpg', 215876, 'image/jpeg', NULL, NULL, 'images', '1', 'Raviraj\\Rjgallery\\Models\\Gallery', 1, 12, '2018-06-28 00:20:02', '2018-06-28 00:22:30'),
	(11, '5b3429b30ece8182307192.jpg', 'KISNA_STAN-77.jpg', 403225, 'image/jpeg', NULL, NULL, 'images', '1', 'Raviraj\\Rjgallery\\Models\\Gallery', 1, 13, '2018-06-28 00:20:03', '2018-06-28 00:22:30'),
	(12, '5b3429b319292047281131.jpg', 'KISNA_STAN-80.jpg', 408342, 'image/jpeg', NULL, NULL, 'images', '1', 'Raviraj\\Rjgallery\\Models\\Gallery', 1, 14, '2018-06-28 00:20:03', '2018-06-28 00:22:30'),
	(13, '5b3429ce8958a102020890.jpg', 'KISNA_STAN-54.jpg', 385837, 'image/jpeg', NULL, NULL, 'images', '1', 'Raviraj\\Rjgallery\\Models\\Gallery', 1, 9, '2018-06-28 00:20:30', '2018-06-28 00:22:30'),
	(14, '5b3429d951d2b569521525.jpg', 'KISNA_STAN-17.jpg', 454091, 'image/jpeg', NULL, NULL, 'images', '1', 'Raviraj\\Rjgallery\\Models\\Gallery', 1, 4, '2018-06-28 00:20:41', '2018-06-28 00:22:30');
/*!40000 ALTER TABLE `system_files` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.system_mail_layouts
CREATE TABLE IF NOT EXISTS `system_mail_layouts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `content_css` text COLLATE utf8mb4_unicode_ci,
  `is_locked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.system_mail_layouts: ~2 rows (approximately)
/*!40000 ALTER TABLE `system_mail_layouts` DISABLE KEYS */;
INSERT INTO `system_mail_layouts` (`id`, `name`, `code`, `content_html`, `content_text`, `content_css`, `is_locked`, `created_at`, `updated_at`) VALUES
	(1, 'Default layout', 'default', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml">\n<head>\n    <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\n</head>\n<body>\n    <style type="text/css" media="screen">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class="wrapper layout-default" width="100%" cellpadding="0" cellspacing="0">\n\n        <!-- Header -->\n        {% partial \'header\' body %}\n            {{ subject|raw }}\n        {% endpartial %}\n\n        <tr>\n            <td align="center">\n                <table class="content" width="100%" cellpadding="0" cellspacing="0">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class="body" width="100%" cellpadding="0" cellspacing="0">\n                            <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class="content-cell">\n                                        {{ content|raw }}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n\n        <!-- Footer -->\n        {% partial \'footer\' body %}\n            &copy; {{ "now"|date("Y") }} {{ appName }}. All rights reserved.\n        {% endpartial %}\n\n    </table>\n\n</body>\n</html>', '{{ content|raw }}', '@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}', 1, '2018-06-27 23:47:48', '2018-06-27 23:47:48'),
	(2, 'System layout', 'system', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml">\n<head>\n    <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\n</head>\n<body>\n    <style type="text/css" media="screen">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class="wrapper layout-system" width="100%" cellpadding="0" cellspacing="0">\n        <tr>\n            <td align="center">\n                <table class="content" width="100%" cellpadding="0" cellspacing="0">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class="body" width="100%" cellpadding="0" cellspacing="0">\n                            <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class="content-cell">\n                                        {{ content|raw }}\n\n                                        <!-- Subcopy -->\n                                        {% partial \'subcopy\' body %}\n                                            **This is an automatic message. Please do not reply to it.**\n                                        {% endpartial %}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n    </table>\n\n</body>\n</html>', '{{ content|raw }}\n\n\n---\nThis is an automatic message. Please do not reply to it.', '@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}', 1, '2018-06-27 23:47:48', '2018-06-27 23:47:48');
/*!40000 ALTER TABLE `system_mail_layouts` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.system_mail_partials
CREATE TABLE IF NOT EXISTS `system_mail_partials` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `is_custom` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.system_mail_partials: ~0 rows (approximately)
/*!40000 ALTER TABLE `system_mail_partials` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_mail_partials` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.system_mail_templates
CREATE TABLE IF NOT EXISTS `system_mail_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `layout_id` int(11) DEFAULT NULL,
  `is_custom` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `system_mail_templates_layout_id_index` (`layout_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.system_mail_templates: ~0 rows (approximately)
/*!40000 ALTER TABLE `system_mail_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_mail_templates` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.system_parameters
CREATE TABLE IF NOT EXISTS `system_parameters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `namespace` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `item_index` (`namespace`,`group`,`item`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.system_parameters: ~8 rows (approximately)
/*!40000 ALTER TABLE `system_parameters` DISABLE KEYS */;
INSERT INTO `system_parameters` (`id`, `namespace`, `group`, `item`, `value`) VALUES
	(1, 'system', 'update', 'count', '0'),
	(2, 'system', 'core', 'build', '"437"'),
	(3, 'system', 'core', 'hash', '"d4a4e1f641e333ff5c26037f86cfe619"'),
	(4, 'system', 'update', 'retry', '1530231061'),
	(5, 'cms', 'theme', 'active', '"forever-bonhommes"'),
	(6, 'system', 'project', 'id', '"0ZGNjAQLgZGp1Awx5YGp5L2WzLGLjAzZ2LmVjZQp2AwZjMTMyLGZ2AmLlLGV2"'),
	(7, 'system', 'project', 'name', '"foreverbonhommes.com"'),
	(8, 'system', 'project', 'owner', '"Stanley Bonhomme"');
/*!40000 ALTER TABLE `system_parameters` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.system_plugin_history
CREATE TABLE IF NOT EXISTS `system_plugin_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `system_plugin_history_code_index` (`code`),
  KEY `system_plugin_history_type_index` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=273 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.system_plugin_history: ~272 rows (approximately)
/*!40000 ALTER TABLE `system_plugin_history` DISABLE KEYS */;
INSERT INTO `system_plugin_history` (`id`, `code`, `type`, `version`, `detail`, `created_at`) VALUES
	(1, 'October.Demo', 'comment', '1.0.1', 'First version of Demo', '2018-06-27 23:47:48'),
	(2, 'Martin.Forms', 'script', '1.0.0', 'create_records_table.php', '2018-06-28 00:13:41'),
	(3, 'Martin.Forms', 'comment', '1.0.0', 'First version of Magic Forms', '2018-06-28 00:13:42'),
	(4, 'Martin.Forms', 'comment', '1.0.1', 'Added CSRF protection', '2018-06-28 00:13:42'),
	(5, 'Martin.Forms', 'comment', '1.1.0', 'Added reCAPTCHA', '2018-06-28 00:13:42'),
	(6, 'Martin.Forms', 'comment', '1.1.1', 'Fix when using reCAPTCHA + allowed fields', '2018-06-28 00:13:42'),
	(7, 'Martin.Forms', 'script', '1.1.2', 'Search inside stored data', '2018-06-28 00:13:42'),
	(8, 'Martin.Forms', 'script', '1.1.2', 'Organize your forms on custom groups', '2018-06-28 00:13:42'),
	(9, 'Martin.Forms', 'script', '1.1.2', 'add_group_field.php', '2018-06-28 00:13:43'),
	(10, 'Martin.Forms', 'comment', '1.1.2', 'Filter forms records', '2018-06-28 00:13:43'),
	(11, 'Martin.Forms', 'comment', '1.2.0', 'Export stored data in CSV format', '2018-06-28 00:13:43'),
	(12, 'Martin.Forms', 'script', '1.2.1', 'Added Turkish language', '2018-06-28 00:13:43'),
	(13, 'Martin.Forms', 'comment', '1.2.1', 'Auto-response email on form submit', '2018-06-28 00:13:43'),
	(14, 'Martin.Forms', 'comment', '1.2.2', 'Override notifications and auto-response email subjects', '2018-06-28 00:13:43'),
	(15, 'Martin.Forms', 'script', '1.2.3', 'Fixed Empty AJAX Form template', '2018-06-28 00:13:43'),
	(16, 'Martin.Forms', 'script', '1.2.3', 'Support for Translate plugin', '2018-06-28 00:13:43'),
	(17, 'Martin.Forms', 'script', '1.2.3', 'Added plugin documentation', '2018-06-28 00:13:43'),
	(18, 'Martin.Forms', 'comment', '1.2.3', 'New option to reset form after successfully submit', '2018-06-28 00:13:43'),
	(19, 'Martin.Forms', 'comment', '1.2.4', 'Added detailed reCAPTCHA help', '2018-06-28 00:13:43'),
	(20, 'Martin.Forms', 'comment', '1.3.0', 'AJAX file uploads', '2018-06-28 00:13:44'),
	(21, 'Martin.Forms', 'comment', '1.3.1', 'Added lang pt-br', '2018-06-28 00:13:44'),
	(22, 'Martin.Forms', 'comment', '1.3.2', 'Fixed multiples reCAPTCHAs on same page', '2018-06-28 00:13:44'),
	(23, 'Martin.Forms', 'script', '1.3.3', 'Updated documentations', '2018-06-28 00:13:44'),
	(24, 'Martin.Forms', 'comment', '1.3.3', 'Fixed record detail page when form data contains an array', '2018-06-28 00:13:44'),
	(25, 'Martin.Forms', 'comment', '1.3.4', 'New "Anonymize IP" option', '2018-06-28 00:13:44'),
	(26, 'Martin.Forms', 'comment', '1.3.5', 'New option "Redirect on successful submit"', '2018-06-28 00:13:44'),
	(27, 'Martin.Forms', 'script', '1.3.6', 'Support Translate plugin on reCAPTCHA', '2018-06-28 00:13:44'),
	(28, 'Martin.Forms', 'script', '1.3.6', 'reCAPTCHA validation enhancements', '2018-06-28 00:13:44'),
	(29, 'Martin.Forms', 'comment', '1.3.6', 'French translation', '2018-06-28 00:13:44'),
	(30, 'Martin.Forms', 'script', '1.3.7', 'Show uploads as list', '2018-06-28 00:13:45'),
	(31, 'Martin.Forms', 'comment', '1.3.7', 'Displaying errors with fields (inline errors)', '2018-06-28 00:13:45'),
	(32, 'Martin.Forms', 'comment', '1.3.8', 'Fixed handling arrays (radio inputs) in notification email', '2018-06-28 00:13:45'),
	(33, 'Martin.Forms', 'script', '1.3.9', 'Execute custom JavaScript on form success or error', '2018-06-28 00:13:45'),
	(34, 'Martin.Forms', 'comment', '1.3.9', 'Use custom mail templates', '2018-06-28 00:13:45'),
	(35, 'Martin.Forms', 'comment', '1.4.0', 'Added Events (please, refer to docs) [thanks to therealkevinard]', '2018-06-28 00:13:45'),
	(36, 'Martin.Forms', 'comment', '1.4.1', 'New option "Reply To"', '2018-06-28 00:13:45'),
	(37, 'Martin.Forms', 'script', '1.4.2', 'New option to sanitize form data (check security docs for more info)', '2018-06-28 00:13:45'),
	(38, 'Martin.Forms', 'script', '1.4.2', 'Added option to send blind carbon copy in notifications email', '2018-06-28 00:13:45'),
	(39, 'Martin.Forms', 'comment', '1.4.2', 'Escape HTML characters on the view records page [thanks to Andre]', '2018-06-28 00:13:45'),
	(40, 'Martin.Forms', 'script', '1.4.3', 'Added "Unread Records" counter', '2018-06-28 00:13:46'),
	(41, 'Martin.Forms', 'script', '1.4.3', 'Fixed errors when only BCC addresses are supplied', '2018-06-28 00:13:46'),
	(42, 'Martin.Forms', 'script', '1.4.3', 'New setting "hide navigation item"', '2018-06-28 00:13:46'),
	(43, 'Martin.Forms', 'script', '1.4.3', 'add_unread_field.php', '2018-06-28 00:13:46'),
	(44, 'Martin.Forms', 'comment', '1.4.3', 'Fixes related to October Build 420', '2018-06-28 00:13:46'),
	(45, 'Martin.Forms', 'comment', '1.4.4', 'Use custom partials for Success and Error messages', '2018-06-28 00:13:46'),
	(46, 'Martin.Forms', 'comment', '1.4.4.1', 'Fix with notifications emails', '2018-06-28 00:13:47'),
	(47, 'Martin.Forms', 'script', '1.4.5', 'Access submited data on auto-response email template', '2018-06-28 00:13:47'),
	(48, 'Martin.Forms', 'comment', '1.4.5', 'Mail class code refactoring', '2018-06-28 00:13:47'),
	(49, 'Martin.Forms', 'comment', '1.4.5.1', 'Store form data without escaping unicode [thanks to panakour]', '2018-06-28 00:13:47'),
	(50, 'Martin.Forms', 'script', '1.4.6', 'Possibility to change the text on the remove file popup [thanks to ShiroeSama]', '2018-06-28 00:13:47'),
	(51, 'Martin.Forms', 'comment', '1.4.6', 'New option to skip saving forms data on database.', '2018-06-28 00:13:47'),
	(52, 'Martin.Forms', 'comment', '1.4.6.1', 'Changed database field from json to text to support MySQL 5.5', '2018-06-28 00:13:47'),
	(53, 'Martin.Forms', 'script', '1.4.7', 'fix custom subject on email template [Thanks to matteotrubini]', '2018-06-28 00:13:47'),
	(54, 'Martin.Forms', 'script', '1.4.7', 'fix email bug when not storing on db [Thanks JurekRaben]', '2018-06-28 00:13:47'),
	(55, 'Martin.Forms', 'script', '1.4.7', 'skip url redirect validation [Thanks to EleRam]', '2018-06-28 00:13:47'),
	(56, 'Martin.Forms', 'comment', '1.4.7', 'you can use your form variables on notification mail subject [thanks to Alex360hd]', '2018-06-28 00:13:47'),
	(57, 'Martin.Forms', 'comment', '1.4.8', 'added GDPR cleanup feature [thanks to Alex360hd]', '2018-06-28 00:13:47'),
	(58, 'Martin.Forms', 'comment', '1.4.9', 'fix on replaceToken function when replacement is null [thanks to leonaze]', '2018-06-28 00:13:47'),
	(59, 'Martin.Forms', 'comment', '1.4.9.1', 'fix a nullable type error on PHP 7.0', '2018-06-28 00:13:47'),
	(60, 'Martin.Forms', 'comment', '1.4.9.2', 'bugfix when a form field array has more than 2 levels of depth', '2018-06-28 00:13:47'),
	(61, 'OFFLINE.ResponsiveImages', 'comment', '1.0.1', 'First version of ResponsiveImages', '2018-06-28 00:13:48'),
	(62, 'OFFLINE.ResponsiveImages', 'script', '1.0.2', 'Fixed broken media manager when using custom backend url', '2018-06-28 00:13:48'),
	(63, 'OFFLINE.ResponsiveImages', 'comment', '1.0.2', 'Fixed encoding problems', '2018-06-28 00:13:48'),
	(64, 'OFFLINE.ResponsiveImages', 'comment', '1.0.3', 'Added alternative-src config option to support jQuery.lazyLoad plugin', '2018-06-28 00:13:48'),
	(65, 'OFFLINE.ResponsiveImages', 'comment', '1.0.4', 'Fixed handling of filenames containing spaces (Thanks to webeks!)', '2018-06-28 00:13:48'),
	(66, 'OFFLINE.ResponsiveImages', 'comment', '1.1.0', 'Added settings page, support for lazy-loading plugins and responsive class attributes', '2018-06-28 00:13:48'),
	(67, 'OFFLINE.ResponsiveImages', 'comment', '1.1.1', 'Added compatibility with current edgeUpdate builds', '2018-06-28 00:13:48'),
	(68, 'OFFLINE.ResponsiveImages', 'comment', '1.1.2', 'Fixed processing of relative pahts when October runs in a subdirectory', '2018-06-28 00:13:48'),
	(69, 'OFFLINE.ResponsiveImages', 'comment', '1.1.3', 'Added french translation (thanks to damsfx)', '2018-06-28 00:13:48'),
	(70, 'OFFLINE.ResponsiveImages', 'comment', '1.1.4', 'Optimized support for multi-byte character strings (thanks to sergei3456)', '2018-06-28 00:13:48'),
	(71, 'OFFLINE.ResponsiveImages', 'comment', '1.1.5', 'Reverted multi-byte optimization since the change removes the DOCTYPE while parsing the html', '2018-06-28 00:13:48'),
	(72, 'RainLab.User', 'script', '1.0.1', 'create_users_table.php', '2018-06-28 00:13:50'),
	(73, 'RainLab.User', 'script', '1.0.1', 'create_throttle_table.php', '2018-06-28 00:13:51'),
	(74, 'RainLab.User', 'comment', '1.0.1', 'Initialize plugin.', '2018-06-28 00:13:51'),
	(75, 'RainLab.User', 'comment', '1.0.2', 'Seed tables.', '2018-06-28 00:13:51'),
	(76, 'RainLab.User', 'comment', '1.0.3', 'Translated hard-coded text to language strings.', '2018-06-28 00:13:51'),
	(77, 'RainLab.User', 'comment', '1.0.4', 'Improvements to user-interface for Location manager.', '2018-06-28 00:13:51'),
	(78, 'RainLab.User', 'comment', '1.0.5', 'Added contact details for users.', '2018-06-28 00:13:51'),
	(79, 'RainLab.User', 'script', '1.0.6', 'create_mail_blockers_table.php', '2018-06-28 00:13:53'),
	(80, 'RainLab.User', 'comment', '1.0.6', 'Added Mail Blocker utility so users can block specific mail templates.', '2018-06-28 00:13:53'),
	(81, 'RainLab.User', 'comment', '1.0.7', 'Add back-end Settings page.', '2018-06-28 00:13:53'),
	(82, 'RainLab.User', 'comment', '1.0.8', 'Updated the Settings page.', '2018-06-28 00:13:53'),
	(83, 'RainLab.User', 'comment', '1.0.9', 'Adds new welcome mail message for users and administrators.', '2018-06-28 00:13:53'),
	(84, 'RainLab.User', 'comment', '1.0.10', 'Adds administrator-only activation mode.', '2018-06-28 00:13:53'),
	(85, 'RainLab.User', 'script', '1.0.11', 'users_add_login_column.php', '2018-06-28 00:13:54'),
	(86, 'RainLab.User', 'comment', '1.0.11', 'Users now have an optional login field that defaults to the email field.', '2018-06-28 00:13:54'),
	(87, 'RainLab.User', 'script', '1.0.12', 'users_rename_login_to_username.php', '2018-06-28 00:13:55'),
	(88, 'RainLab.User', 'comment', '1.0.12', 'Create a dedicated setting for choosing the login mode.', '2018-06-28 00:13:55'),
	(89, 'RainLab.User', 'comment', '1.0.13', 'Minor fix to the Account sign in logic.', '2018-06-28 00:13:55'),
	(90, 'RainLab.User', 'comment', '1.0.14', 'Minor improvements to the code.', '2018-06-28 00:13:55'),
	(91, 'RainLab.User', 'script', '1.0.15', 'users_add_surname.php', '2018-06-28 00:13:56'),
	(92, 'RainLab.User', 'comment', '1.0.15', 'Adds last name column to users table (surname).', '2018-06-28 00:13:56'),
	(93, 'RainLab.User', 'comment', '1.0.16', 'Require permissions for settings page too.', '2018-06-28 00:13:56'),
	(94, 'RainLab.User', 'comment', '1.1.0', '!!! Profile fields and Locations have been removed.', '2018-06-28 00:13:56'),
	(95, 'RainLab.User', 'script', '1.1.1', 'create_user_groups_table.php', '2018-06-28 00:13:58'),
	(96, 'RainLab.User', 'script', '1.1.1', 'seed_user_groups_table.php', '2018-06-28 00:13:58'),
	(97, 'RainLab.User', 'comment', '1.1.1', 'Users can now be added to groups.', '2018-06-28 00:13:58'),
	(98, 'RainLab.User', 'comment', '1.1.2', 'A raw URL can now be passed as the redirect property in the Account component.', '2018-06-28 00:13:58'),
	(99, 'RainLab.User', 'comment', '1.1.3', 'Adds a super user flag to the users table, reserved for future use.', '2018-06-28 00:13:58'),
	(100, 'RainLab.User', 'comment', '1.1.4', 'User list can be filtered by the group they belong to.', '2018-06-28 00:13:59'),
	(101, 'RainLab.User', 'comment', '1.1.5', 'Adds a new permission to hide the User settings menu item.', '2018-06-28 00:13:59'),
	(102, 'RainLab.User', 'script', '1.2.0', 'users_add_deleted_at.php', '2018-06-28 00:14:00'),
	(103, 'RainLab.User', 'comment', '1.2.0', 'Users can now deactivate their own accounts.', '2018-06-28 00:14:00'),
	(104, 'RainLab.User', 'comment', '1.2.1', 'New feature for checking if a user is recently active/online.', '2018-06-28 00:14:00'),
	(105, 'RainLab.User', 'comment', '1.2.2', 'Add bulk action button to user list.', '2018-06-28 00:14:00'),
	(106, 'RainLab.User', 'comment', '1.2.3', 'Included some descriptive paragraphs in the Reset Password component markup.', '2018-06-28 00:14:00'),
	(107, 'RainLab.User', 'comment', '1.2.4', 'Added a checkbox for blocking all mail sent to the user.', '2018-06-28 00:14:00'),
	(108, 'RainLab.User', 'script', '1.2.5', 'update_timestamp_nullable.php', '2018-06-28 00:14:00'),
	(109, 'RainLab.User', 'comment', '1.2.5', 'Database maintenance. Updated all timestamp columns to be nullable.', '2018-06-28 00:14:01'),
	(110, 'RainLab.User', 'script', '1.2.6', 'users_add_last_seen.php', '2018-06-28 00:14:01'),
	(111, 'RainLab.User', 'comment', '1.2.6', 'Add a dedicated last seen column for users.', '2018-06-28 00:14:02'),
	(112, 'RainLab.User', 'comment', '1.2.7', 'Minor fix to user timestamp attributes.', '2018-06-28 00:14:02'),
	(113, 'RainLab.User', 'comment', '1.2.8', 'Add date range filter to users list. Introduced a logout event.', '2018-06-28 00:14:02'),
	(114, 'RainLab.User', 'comment', '1.2.9', 'Add invitation mail for new accounts created in the back-end.', '2018-06-28 00:14:02'),
	(115, 'RainLab.User', 'script', '1.3.0', 'users_add_guest_flag.php', '2018-06-28 00:14:03'),
	(116, 'RainLab.User', 'script', '1.3.0', 'users_add_superuser_flag.php', '2018-06-28 00:14:04'),
	(117, 'RainLab.User', 'comment', '1.3.0', 'Introduced guest user accounts.', '2018-06-28 00:14:04'),
	(118, 'RainLab.User', 'comment', '1.3.1', 'User notification variables can now be extended.', '2018-06-28 00:14:04'),
	(119, 'RainLab.User', 'comment', '1.3.2', 'Minor fix to the Auth::register method.', '2018-06-28 00:14:04'),
	(120, 'RainLab.User', 'comment', '1.3.3', 'Allow prevention of concurrent user sessions via the user settings.', '2018-06-28 00:14:04'),
	(121, 'RainLab.User', 'comment', '1.3.4', 'Added force secure protocol property to the account component.', '2018-06-28 00:14:04'),
	(122, 'RainLab.User', 'comment', '1.4.0', '!!! The Notifications tab in User settings has been removed.', '2018-06-28 00:14:04'),
	(123, 'RainLab.User', 'comment', '1.4.1', 'Added support for user impersonation.', '2018-06-28 00:14:05'),
	(124, 'RainLab.User', 'comment', '1.4.2', 'Fixes security bug in Password Reset component.', '2018-06-28 00:14:05'),
	(125, 'RainLab.User', 'comment', '1.4.3', 'Fixes session handling for AJAX requests.', '2018-06-28 00:14:05'),
	(126, 'RainLab.User', 'comment', '1.4.4', 'Fixes bug where impersonation touches the last seen timestamp.', '2018-06-28 00:14:05'),
	(127, 'RainLab.User', 'comment', '1.4.5', 'Added token fallback process to Account / Reset Password components when parameter is missing.', '2018-06-28 00:14:05'),
	(128, 'RainLab.User', 'comment', '1.4.6', 'Fixes Auth::register method signature mismatch with core OctoberCMS Auth library', '2018-06-28 00:14:05'),
	(129, 'Raviraj.Rjgallery', 'script', '1.0.1', 'create_galleries_table.php', '2018-06-28 00:14:06'),
	(130, 'Raviraj.Rjgallery', 'comment', '1.0.1', 'Initial version of RjGallary.', '2018-06-28 00:14:06'),
	(131, 'Raviraj.Rjgallery', 'comment', '1.0.2', 'jQuery inject option added to component options.', '2018-06-28 00:14:06'),
	(132, 'Raviraj.Rjgallery', 'comment', '1.0.3', 'Minor bug fix.', '2018-06-28 00:14:06'),
	(133, 'Raviraj.Rjgallery', 'comment', '1.0.4', 'Can now specify thumbnail resizer mode.', '2018-06-28 00:14:06'),
	(134, 'Raviraj.Rjgallery', 'comment', '1.0.5', 'Multiple language support added.', '2018-06-28 00:14:06'),
	(135, 'Raviraj.Rjgallery', 'comment', '1.0.6', 'Minor fix.', '2018-06-28 00:14:06'),
	(136, 'Raviraj.Rjgallery', 'comment', '1.0.7', 'Added compatibility for latest version of OctoberCMS.', '2018-06-28 00:14:06'),
	(137, 'Raviraj.Rjgallery', 'comment', '1.0.8', 'Add permissions management', '2018-06-28 00:14:06'),
	(138, 'Raviraj.Rjgallery', 'comment', '1.0.9', 'Add CS locale', '2018-06-28 00:14:06'),
	(139, 'Raviraj.Rjgallery', 'comment', '1.1.0', 'Fix deleting galleries', '2018-06-28 00:14:06'),
	(140, 'Raviraj.Rjgallery', 'comment', '1.1.1', 'Fix config path for OctoberCMS build 324', '2018-06-28 00:14:06'),
	(141, 'Raviraj.Rjgallery', 'script', '1.1.2', 'galleries_add_desc_field_table.php', '2018-06-28 00:14:07'),
	(142, 'Raviraj.Rjgallery', 'comment', '1.1.2', 'Add slug and description in gallery model.', '2018-06-28 00:14:07'),
	(143, 'Raviraj.Rjgallery', 'script', '1.1.3', 'create_categories_table.php', '2018-06-28 00:14:09'),
	(144, 'Raviraj.Rjgallery', 'comment', '1.1.3', 'Add Category database and Category model.', '2018-06-28 00:14:09'),
	(145, 'Raviraj.Rjgallery', 'comment', '1.2.0', 'Light gallery upgraded to v1.4, Several code optimizations', '2018-06-28 00:14:09'),
	(146, 'Raviraj.Rjgallery', 'comment', '1.2.1', 'Hot fix asset loading', '2018-06-28 00:14:10'),
	(147, 'Raviraj.Rjgallery', 'comment', '1.2.2', 'Page snippets registered', '2018-06-28 00:14:10'),
	(148, 'Clake.UserExtended', 'script', '1.0.1', 'users_groups_refactor.php', '2018-06-28 00:14:11'),
	(149, 'Clake.UserExtended', 'script', '1.0.1', 'seed_basic_user_groups.php', '2018-06-28 00:14:11'),
	(150, 'Clake.UserExtended', 'comment', '1.0.1', 'Seed in basic groups.', '2018-06-28 00:14:11'),
	(151, 'Clake.UserExtended', 'script', '1.0.2', 'create_friends_table.php', '2018-06-28 00:14:12'),
	(152, 'Clake.UserExtended', 'comment', '1.0.2', 'Create the table.', '2018-06-28 00:14:12'),
	(153, 'Clake.UserExtended', 'script', '1.0.3', 'friends_add_accepted.php', '2018-06-28 00:14:13'),
	(154, 'Clake.UserExtended', 'comment', '1.0.3', 'Add Accepted to Friends table.', '2018-06-28 00:14:13'),
	(155, 'Clake.UserExtended', 'comment', '1.0.4', 'Added user searching', '2018-06-28 00:14:13'),
	(156, 'Clake.UserExtended', 'script', '1.0.5', 'create_comments_table.php', '2018-06-28 00:14:14'),
	(157, 'Clake.UserExtended', 'comment', '1.0.5', 'Add Comments table', '2018-06-28 00:14:14'),
	(158, 'Clake.UserExtended', 'script', '1.0.6', 'comments_add_author.php', '2018-06-28 00:14:14'),
	(159, 'Clake.UserExtended', 'comment', '1.0.6', 'Added author to comments table', '2018-06-28 00:14:14'),
	(160, 'Clake.UserExtended', 'comment', '1.0.7', 'Added user profile pages', '2018-06-28 00:14:14'),
	(161, 'Clake.UserExtended', 'comment', '1.0.8', 'Added delete capabilities for comments and friends', '2018-06-28 00:14:15'),
	(162, 'Clake.UserExtended', 'comment', '1.0.9', 'Fixed \'Code Already In use\' bug', '2018-06-28 00:14:15'),
	(163, 'Clake.UserExtended', 'script', '1.0.10', 'user_groups_add_level.php', '2018-06-28 00:14:16'),
	(164, 'Clake.UserExtended', 'comment', '1.0.10', 'Added Heirarchal Level to User Groups', '2018-06-28 00:14:16'),
	(165, 'Clake.UserExtended', 'comment', '1.0.11', 'Updated User Group Seeder. You may wish to consider to rerun it.', '2018-06-28 00:14:16'),
	(166, 'Clake.UserExtended', 'script', '1.0.12', 'create_timezones_table.php', '2018-06-28 00:14:16'),
	(167, 'Clake.UserExtended', 'comment', '1.0.12', 'Added Timezone table', '2018-06-28 00:14:16'),
	(168, 'Clake.UserExtended', 'script', '1.0.13', 'user_add_timezone.php', '2018-06-28 00:14:17'),
	(169, 'Clake.UserExtended', 'comment', '1.0.13', 'Modified Users table to add a timezone_id column', '2018-06-28 00:14:18'),
	(170, 'Clake.UserExtended', 'script', '1.0.14', 'seed_time_zones.php', '2018-06-28 00:14:18'),
	(171, 'Clake.UserExtended', 'comment', '1.0.14', 'Seed timezones', '2018-06-28 00:14:18'),
	(172, 'Clake.UserExtended', 'script', '1.0.15', 'create_roles_table.php', '2018-06-28 00:14:18'),
	(173, 'Clake.UserExtended', 'comment', '1.0.15', 'Create Roles table', '2018-06-28 00:14:18'),
	(174, 'Clake.UserExtended', 'script', '1.0.16', 'Use Case 1; You own a blogging company with a group called \'writers\'. The writers group can have roles \'Senior Writer\', \'Junior Writer\', and \'Writer Editor\'', '2018-06-28 00:14:18'),
	(175, 'Clake.UserExtended', 'comment', '1.0.16', 'Added Roles as a linear heirarchy under a group.', '2018-06-28 00:14:18'),
	(176, 'Clake.UserExtended', 'script', '1.0.17', 'user_groups_add_role.php', '2018-06-28 00:14:19'),
	(177, 'Clake.UserExtended', 'comment', '1.0.17', 'Adds role to the Users Groups table', '2018-06-28 00:14:19'),
	(178, 'Clake.UserExtended', 'comment', '1.0.18', 'Removes existing primary keys from Users Groups model. Removed script as it was breaking.', '2018-06-28 00:14:19'),
	(179, 'Clake.UserExtended', 'comment', '1.0.19', 'Adds a primary id key to the users_groups model. Removed script as it was breaking.', '2018-06-28 00:14:19'),
	(180, 'Clake.UserExtended', 'comment', '1.0.20', 'Added a controller for Roles. Check the \'User\' tab in the backend.', '2018-06-28 00:14:19'),
	(181, 'Clake.UserExtended', 'comment', '1.0.21', 'Added the Timezonable trait. Useful for automatically converting model fields to the logged in users timezone.', '2018-06-28 00:14:20'),
	(182, 'Clake.UserExtended', 'comment', '1.0.22', 'Initial release of the Role Manager in the backend.', '2018-06-28 00:14:20'),
	(183, 'Clake.UserExtended', 'script', '1.0.23', 'user_groups_change_level_to_sort_order.php', '2018-06-28 00:14:20'),
	(184, 'Clake.UserExtended', 'comment', '1.0.23', 'Bug Fixes', '2018-06-28 00:14:20'),
	(185, 'Clake.UserExtended', 'comment', '1.0.24', 'Fixed data structures bug', '2018-06-28 00:14:20'),
	(186, 'Clake.UserExtended', 'script', '1.0.25', 'user_add_settings.php', '2018-06-28 00:14:21'),
	(187, 'Clake.UserExtended', 'comment', '1.0.25', 'Add user settings', '2018-06-28 00:14:21'),
	(188, 'Clake.UserExtended', 'comment', '1.1.00', 'User Extended Beta release. Check your project as many things are now Deprecated and rewritten.', '2018-06-28 00:14:22'),
	(189, 'Clake.UserExtended', 'comment', '1.1.01', 'Creating role bug fix', '2018-06-28 00:14:22'),
	(190, 'Clake.UserExtended', 'comment', '1.1.02', 'Fixed non-property error', '2018-06-28 00:14:22'),
	(191, 'Clake.UserExtended', 'comment', '1.1.03', 'Fixed undefined offset error', '2018-06-28 00:14:22'),
	(192, 'Clake.UserExtended', 'comment', '2.0.00', 'User Extended Core Stable Release. Check your project as many things are now Deprecated and rewritten and will be removed in version 3.0.00', '2018-06-28 00:14:22'),
	(193, 'Clake.UserExtended', 'script', '2.0.01', 'create_fields_table.php', '2018-06-28 00:14:23'),
	(194, 'Clake.UserExtended', 'comment', '2.0.01', 'Began work on 2.1.00 and creating fields table', '2018-06-28 00:14:23'),
	(195, 'Clake.UserExtended', 'script', '2.0.02', 'friends_add_relation.php', '2018-06-28 00:14:24'),
	(196, 'Clake.UserExtended', 'comment', '2.0.02', 'Adding relation column to the Friends model. This is being shipped now in preparation of 2.2.00', '2018-06-28 00:14:24'),
	(197, 'Clake.UserExtended', 'comment', '2.0.03', 'Implemented Fields Manager', '2018-06-28 00:14:24'),
	(198, 'Clake.UserExtended', 'script', '2.0.04', 'create_routes_table.php', '2018-06-28 00:14:24'),
	(199, 'Clake.UserExtended', 'comment', '2.0.04', 'Created Routes table. This is being shipped now in preparation of 2.2.00', '2018-06-28 00:14:24'),
	(200, 'Clake.UserExtended', 'script', '2.0.05', 'create_route_restriction_table.php', '2018-06-28 00:14:25'),
	(201, 'Clake.UserExtended', 'comment', '2.0.05', 'Created Route Restriction Table. This is being shipped now in preparation of 2.2.00', '2018-06-28 00:14:25'),
	(202, 'Clake.UserExtended', 'comment', '2.0.06', 'Completed RoleManager refactor and added drag&drop and pagination', '2018-06-28 00:14:25'),
	(203, 'Clake.UserExtended', 'script', '2.0.07', 'seed_fields.php', '2018-06-28 00:14:25'),
	(204, 'Clake.UserExtended', 'comment', '2.0.07', 'Added nicknames. This is being shipped now in preparation of 2.2.00', '2018-06-28 00:14:25'),
	(205, 'Clake.UserExtended', 'script', '2.0.08', 'create_integrated_users.php', '2018-06-28 00:14:26'),
	(206, 'Clake.UserExtended', 'comment', '2.0.08', 'Added integrated users. This is being shipped in preparation for 2.4.00 and 2.5.00', '2018-06-28 00:14:26'),
	(207, 'Clake.UserExtended', 'comment', '2.1.00', 'User Extended Core v2.1.00 Release. Deprecated components and functions have been removed.', '2018-06-28 00:14:26'),
	(208, 'Clake.UserExtended', 'comment', '2.1.01', 'Fixed a bug which was preventing autofilling of custom fields. Oops.', '2018-06-28 00:14:26'),
	(209, 'Clake.UserExtended', 'comment', '2.1.02', 'Fixed a bug which was causing issues with the Field Manager', '2018-06-28 00:14:26'),
	(210, 'Clake.UserExtended', 'comment', '2.1.03', 'Fixed a JS bug with UE not defined and interact not defined', '2018-06-28 00:14:26'),
	(211, 'Clake.UserExtended', 'script', '2.1.04', 'route_restriction_add_deletes_attempts.php', '2018-06-28 00:14:28'),
	(212, 'Clake.UserExtended', 'comment', '2.1.04', 'Tweaked route restriction table', '2018-06-28 00:14:28'),
	(213, 'Clake.UserExtended', 'script', '2.1.05', 'routes_switch_type.php', '2018-06-28 00:14:30'),
	(214, 'Clake.UserExtended', 'comment', '2.1.05', 'Removed type from routes and replaced it with an enabled flag', '2018-06-28 00:14:30'),
	(215, 'Clake.UserExtended', 'script', '2.1.06', 'route_restriction_add_flags.php', '2018-06-28 00:14:31'),
	(216, 'Clake.UserExtended', 'comment', '2.1.06', 'Added additional fields for route resitrctions', '2018-06-28 00:14:31'),
	(217, 'Clake.UserExtended', 'script', '2.1.07', 'create_routes_pivot_table.php', '2018-06-28 00:14:33'),
	(218, 'Clake.UserExtended', 'comment', '2.1.07', 'Added a pivot table for route restrictions', '2018-06-28 00:14:33'),
	(219, 'Clake.UserExtended', 'script', '2.1.08', 'routes_add_description_and_child.php', '2018-06-28 00:14:34'),
	(220, 'Clake.UserExtended', 'comment', '2.1.08', 'Added descriptions and cascading to children for route restricitons', '2018-06-28 00:14:34'),
	(221, 'Clake.UserExtended', 'script', '2.1.09', 'users_groups_add_timestamps.php', '2018-06-28 00:14:35'),
	(222, 'Clake.UserExtended', 'comment', '2.1.09', 'Added created_at, deleted_at, updated_at to UsersGroups', '2018-06-28 00:14:35'),
	(223, 'Clake.UserExtended', 'script', '2.1.10', 'seed_account_settings.php', '2018-06-28 00:14:36'),
	(224, 'Clake.UserExtended', 'comment', '2.1.10', 'Added some core settings fields for handing bans and suspensions', '2018-06-28 00:14:36'),
	(225, 'Clake.UserExtended', 'script', '2.1.11', 'create_modules_table.php', '2018-06-28 00:14:36'),
	(226, 'Clake.UserExtended', 'comment', '2.1.11', 'Started work on the Module Manager. This is being shipped in preparation of 2.3.00', '2018-06-28 00:14:36'),
	(227, 'Clake.UserExtended', 'comment', '2.2.00', 'User Extended Core v2.2.00 Release. \'The Backend Update\' adds Route Restrictions, Friend management, comment management, timezone management, and improved field management.', '2018-06-28 00:14:37'),
	(228, 'DmitryBykov.Htmlminify', 'comment', '1.0.1', 'HTML minify plugin', '2018-06-28 00:17:27'),
	(229, 'RainLab.Pages', 'comment', '1.0.1', 'Implemented the static pages management and the Static Page component.', '2018-06-28 00:21:32'),
	(230, 'RainLab.Pages', 'comment', '1.0.2', 'Fixed the page preview URL.', '2018-06-28 00:21:33'),
	(231, 'RainLab.Pages', 'comment', '1.0.3', 'Implemented menus.', '2018-06-28 00:21:34'),
	(232, 'RainLab.Pages', 'comment', '1.0.4', 'Implemented the content block management and placeholder support.', '2018-06-28 00:21:34'),
	(233, 'RainLab.Pages', 'comment', '1.0.5', 'Added support for the Sitemap plugin.', '2018-06-28 00:21:34'),
	(234, 'RainLab.Pages', 'comment', '1.0.6', 'Minor updates to the internal API.', '2018-06-28 00:21:34'),
	(235, 'RainLab.Pages', 'comment', '1.0.7', 'Added the Snippets feature.', '2018-06-28 00:21:34'),
	(236, 'RainLab.Pages', 'comment', '1.0.8', 'Minor improvements to the code.', '2018-06-28 00:21:34'),
	(237, 'RainLab.Pages', 'comment', '1.0.9', 'Fixes issue where Snippet tab is missing from the Partials form.', '2018-06-28 00:21:34'),
	(238, 'RainLab.Pages', 'comment', '1.0.10', 'Add translations for various locales.', '2018-06-28 00:21:34'),
	(239, 'RainLab.Pages', 'comment', '1.0.11', 'Fixes issue where placeholders tabs were missing from Page form.', '2018-06-28 00:21:34'),
	(240, 'RainLab.Pages', 'comment', '1.0.12', 'Implement Media Manager support.', '2018-06-28 00:21:34'),
	(241, 'RainLab.Pages', 'script', '1.1.0', 'snippets_rename_viewbag_properties.php', '2018-06-28 00:21:35'),
	(242, 'RainLab.Pages', 'comment', '1.1.0', 'Adds meta title and description to pages. Adds |staticPage filter.', '2018-06-28 00:21:35'),
	(243, 'RainLab.Pages', 'comment', '1.1.1', 'Add support for Syntax Fields.', '2018-06-28 00:21:35'),
	(244, 'RainLab.Pages', 'comment', '1.1.2', 'Static Breadcrumbs component now respects the hide from navigation setting.', '2018-06-28 00:21:35'),
	(245, 'RainLab.Pages', 'comment', '1.1.3', 'Minor back-end styling fix.', '2018-06-28 00:21:35'),
	(246, 'RainLab.Pages', 'comment', '1.1.4', 'Minor fix to the StaticPage component API.', '2018-06-28 00:21:35'),
	(247, 'RainLab.Pages', 'comment', '1.1.5', 'Fixes bug when using syntax fields.', '2018-06-28 00:21:35'),
	(248, 'RainLab.Pages', 'comment', '1.1.6', 'Minor styling fix to the back-end UI.', '2018-06-28 00:21:35'),
	(249, 'RainLab.Pages', 'comment', '1.1.7', 'Improved menu item form to include CSS class, open in a new window and hidden flag.', '2018-06-28 00:21:35'),
	(250, 'RainLab.Pages', 'comment', '1.1.8', 'Improved the output of snippet partials when saved.', '2018-06-28 00:21:35'),
	(251, 'RainLab.Pages', 'comment', '1.1.9', 'Minor update to snippet inspector internal API.', '2018-06-28 00:21:35'),
	(252, 'RainLab.Pages', 'comment', '1.1.10', 'Fixes a bug where selecting a layout causes permanent unsaved changes.', '2018-06-28 00:21:35'),
	(253, 'RainLab.Pages', 'comment', '1.1.11', 'Add support for repeater syntax field.', '2018-06-28 00:21:36'),
	(254, 'RainLab.Pages', 'comment', '1.2.0', 'Added support for translations, UI updates.', '2018-06-28 00:21:36'),
	(255, 'RainLab.Pages', 'comment', '1.2.1', 'Use nice titles when listing the content files.', '2018-06-28 00:21:36'),
	(256, 'RainLab.Pages', 'comment', '1.2.2', 'Minor styling update.', '2018-06-28 00:21:36'),
	(257, 'RainLab.Pages', 'comment', '1.2.3', 'Snippets can now be moved by dragging them.', '2018-06-28 00:21:36'),
	(258, 'RainLab.Pages', 'comment', '1.2.4', 'Fixes a bug where the cursor is misplaced when editing text files.', '2018-06-28 00:21:36'),
	(259, 'RainLab.Pages', 'comment', '1.2.5', 'Fixes a bug where the parent page is lost upon changing a page layout.', '2018-06-28 00:21:36'),
	(260, 'RainLab.Pages', 'comment', '1.2.6', 'Shared view variables are now passed to static pages.', '2018-06-28 00:21:36'),
	(261, 'RainLab.Pages', 'comment', '1.2.7', 'Fixes issue with duplicating properties when adding multiple snippets on the same page.', '2018-06-28 00:21:36'),
	(262, 'RainLab.Pages', 'comment', '1.2.8', 'Fixes a bug where creating a content block without extension doesn\'t save the contents to file.', '2018-06-28 00:21:36'),
	(263, 'RainLab.Pages', 'comment', '1.2.9', 'Add conditional support for translating page URLs.', '2018-06-28 00:21:36'),
	(264, 'RainLab.Pages', 'comment', '1.2.10', 'Streamline generation of URLs to use the new Cms::url helper.', '2018-06-28 00:21:36'),
	(265, 'RainLab.Pages', 'comment', '1.2.11', 'Implements repeater usage with translate plugin.', '2018-06-28 00:21:36'),
	(266, 'RainLab.Pages', 'comment', '1.2.12', 'Fixes minor issue when using snippets and switching the application locale.', '2018-06-28 00:21:37'),
	(267, 'RainLab.Pages', 'comment', '1.2.13', 'Fixes bug when AJAX is used on a page that does not yet exist.', '2018-06-28 00:21:37'),
	(268, 'RainLab.Pages', 'comment', '1.2.14', 'Add theme logging support for changes made to menus.', '2018-06-28 00:21:37'),
	(269, 'RainLab.Pages', 'comment', '1.2.15', 'Back-end navigation sort order updated.', '2018-06-28 00:21:37'),
	(270, 'RainLab.Pages', 'comment', '1.2.16', 'Fixes a bug when saving a template that has been modified outside of the CMS (mtime mismatch).', '2018-06-28 00:21:37'),
	(271, 'RainLab.Pages', 'comment', '1.2.17', 'Changes locations of custom fields to secondary tabs instead of the primary Settings area. New menu search ability on adding menu items', '2018-06-28 00:21:37'),
	(272, 'RainLab.Pages', 'comment', '1.2.18', 'Fixes cache-invalidation issues when RainLab.Translate is not installed. Added Greek & Simplified Chinese translations. Removed deprecated calls. Allowed saving HTML in snippet properties. Added support for the MediaFinder in menu items.', '2018-06-28 00:21:37'),
	(273, 'Indikator.DevTools', 'comment', '1.0.0', 'First version of Developer Tools.', '2018-06-28 18:03:33'),
	(274, 'Indikator.DevTools', 'comment', '1.1.0', 'Edit plugins with the code editor.', '2018-06-28 18:03:33'),
	(275, 'Indikator.DevTools', 'comment', '1.1.1', 'Translate some English texts.', '2018-06-28 18:03:34'),
	(276, 'Indikator.DevTools', 'comment', '1.1.2', 'Fixed the Create file issue.', '2018-06-28 18:03:34'),
	(277, 'Indikator.DevTools', 'comment', '1.1.3', 'Added new icon for main navigation.', '2018-06-28 18:03:34'),
	(278, 'Indikator.DevTools', 'comment', '1.1.4', 'Show the PHP\'s configuration.', '2018-06-28 18:03:34'),
	(279, 'Indikator.DevTools', 'comment', '1.1.5', 'Minor code improvements and bugfix.', '2018-06-28 18:03:34'),
	(280, 'Indikator.DevTools', 'comment', '1.1.6', 'The top menu icon shows again.', '2018-06-28 18:03:34'),
	(281, 'Indikator.DevTools', 'comment', '1.1.7', 'Fixed the Create folder issue.', '2018-06-28 18:03:34');
/*!40000 ALTER TABLE `system_plugin_history` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.system_plugin_versions
CREATE TABLE IF NOT EXISTS `system_plugin_versions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT '0',
  `is_frozen` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `system_plugin_versions_code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.system_plugin_versions: ~8 rows (approximately)
/*!40000 ALTER TABLE `system_plugin_versions` DISABLE KEYS */;
INSERT INTO `system_plugin_versions` (`id`, `code`, `version`, `created_at`, `is_disabled`, `is_frozen`) VALUES
	(1, 'October.Demo', '1.0.1', '2018-06-27 23:47:48', 0, 0),
	(2, 'Martin.Forms', '1.4.9.2', '2018-06-28 00:13:47', 0, 0),
	(3, 'OFFLINE.ResponsiveImages', '1.1.5', '2018-06-28 00:13:48', 0, 0),
	(4, 'RainLab.User', '1.4.6', '2018-06-28 00:14:05', 0, 0),
	(5, 'Raviraj.Rjgallery', '1.2.2', '2018-06-28 00:14:10', 0, 0),
	(6, 'Clake.UserExtended', '2.2.00', '2018-06-28 00:14:37', 0, 0),
	(7, 'DmitryBykov.Htmlminify', '1.0.1', '2018-06-28 00:17:28', 0, 0),
	(8, 'RainLab.Pages', '1.2.18', '2018-06-28 00:21:37', 0, 0),
	(9, 'Indikator.DevTools', '1.1.7', '2018-06-28 18:03:34', 0, 0);
/*!40000 ALTER TABLE `system_plugin_versions` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.system_request_logs
CREATE TABLE IF NOT EXISTS `system_request_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status_code` int(11) DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` text COLLATE utf8mb4_unicode_ci,
  `count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.system_request_logs: ~0 rows (approximately)
/*!40000 ALTER TABLE `system_request_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_request_logs` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.system_revisions
CREATE TABLE IF NOT EXISTS `system_revisions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cast` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_value` text COLLATE utf8mb4_unicode_ci,
  `new_value` text COLLATE utf8mb4_unicode_ci,
  `revisionable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revisionable_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `system_revisions_revisionable_id_revisionable_type_index` (`revisionable_id`,`revisionable_type`),
  KEY `system_revisions_user_id_index` (`user_id`),
  KEY `system_revisions_field_index` (`field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.system_revisions: ~0 rows (approximately)
/*!40000 ALTER TABLE `system_revisions` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_revisions` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.system_settings
CREATE TABLE IF NOT EXISTS `system_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `system_settings_item_index` (`item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.system_settings: ~0 rows (approximately)
/*!40000 ALTER TABLE `system_settings` DISABLE KEYS */;
INSERT INTO `system_settings` (`id`, `item`, `value`) VALUES
	(1, 'clake_userextended_settings', '{"validation_username":"required|between:2,255|unique:users,username","validation_password":"required|between:4,255|confirmed","enable_profiles":"1","enable_friends":"1","enable_timezones":"1","enable_groups":"1","enable_email":"1","enable_disqus":"0","enable_facebook":"0","disqus_shortname":"","facebook_appid":"","default_timezone":"29","default_group":"guest","closing_deletes":"0","track_route_attempts":"1","dev_mode":"1"}'),
	(2, 'user_settings', '{"require_activation":"1","activate_mode":"auto","use_throttle":"1","block_persistence":"0","allow_registration":"1","login_attribute":"username"}'),
	(3, 'system_mail_settings', '{"send_mode":"mail","sender_name":"Forever Bonhommes","sender_email":"artmanstan@gmail.com","sendmail_path":"\\/usr\\/sbin\\/sendmail -bs","smtp_address":"smtp.mailgun.org","smtp_port":"587","smtp_user":"","smtp_password":"","smtp_authorization":"0","smtp_encryption":"tls","mailgun_domain":"","mailgun_secret":"","mandrill_secret":"","ses_key":"","ses_secret":"","ses_region":""}');
/*!40000 ALTER TABLE `system_settings` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persist_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `is_activated` tinyint(1) NOT NULL DEFAULT '0',
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `last_seen` timestamp NULL DEFAULT NULL,
  `is_guest` tinyint(1) NOT NULL DEFAULT '0',
  `is_superuser` tinyint(1) NOT NULL DEFAULT '0',
  `timezone_id` int(11) NOT NULL DEFAULT '1',
  `settings` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_login_unique` (`username`),
  KEY `users_activation_code_index` (`activation_code`),
  KEY `users_reset_password_code_index` (`reset_password_code`),
  KEY `users_login_index` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `activation_code`, `persist_code`, `reset_password_code`, `permissions`, `is_activated`, `activated_at`, `last_login`, `created_at`, `updated_at`, `username`, `surname`, `deleted_at`, `last_seen`, `is_guest`, `is_superuser`, `timezone_id`, `settings`) VALUES
	(1, 'Guest', 'artmanstan@gmail.com', '$2y$10$aZ.XdzHI5dIW2ynFnbktpOieg3Gf2Fo1AshmoDfW85UrN3Jdrl2EC', NULL, '$2y$10$u3Kx2f3Xf49/hCL2irIMb.M5qhCC.IhwJimag1f38YCSiN6fjSp9i', NULL, NULL, 1, '2018-06-28 18:04:26', '2018-06-28 21:00:01', '2018-06-28 02:45:01', '2018-06-28 21:00:01', 'guest', '', NULL, NULL, 0, 0, 1, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.users_groups
CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `user_group_id` int(10) unsigned NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.users_groups: ~0 rows (approximately)
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;
INSERT INTO `users_groups` (`id`, `user_id`, `user_group_id`, `role_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 3, 0, NULL, NULL, NULL);
/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.user_groups
CREATE TABLE IF NOT EXISTS `user_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_groups_code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.user_groups: ~7 rows (approximately)
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;
INSERT INTO `user_groups` (`id`, `name`, `code`, `description`, `created_at`, `updated_at`, `sort_order`) VALUES
	(1, 'Admin', 'admin', 'Administrator group', '2018-06-28 00:14:11', '2018-06-28 00:14:11', 0),
	(2, 'Friend', 'friend', 'Generalized friend group.', '2018-06-28 00:14:11', '2018-06-28 00:14:11', 0),
	(3, 'Guest', 'guest', 'Generalized guest group', '2018-06-28 00:14:11', '2018-06-28 00:14:11', 0),
	(4, 'Tester', 'tester', 'Access bleeding edge features', '2018-06-28 00:14:11', '2018-06-28 00:14:11', 0),
	(5, 'Debugger', 'debugger', 'Debug text, buttons, and visuals appear on the pages', '2018-06-28 00:14:11', '2018-06-28 00:14:11', 0),
	(6, 'Developer', 'developer', 'Access to the dev tools and options', '2018-06-28 00:14:11', '2018-06-28 00:14:11', 0),
	(7, 'Banned', 'banned', 'Banned from viewing pages', '2018-06-28 00:14:11', '2018-06-28 00:14:11', 0);
/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;

-- Dumping structure for table foreverbonhommes.user_throttle
CREATE TABLE IF NOT EXISTS `user_throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `is_suspended` tinyint(1) NOT NULL DEFAULT '0',
  `suspended_at` timestamp NULL DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  `banned_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_throttle_user_id_index` (`user_id`),
  KEY `user_throttle_ip_address_index` (`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table foreverbonhommes.user_throttle: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_throttle` DISABLE KEYS */;
INSERT INTO `user_throttle` (`id`, `user_id`, `ip_address`, `attempts`, `last_attempt_at`, `is_suspended`, `suspended_at`, `is_banned`, `banned_at`) VALUES
	(1, 1, '127.0.0.1', 0, NULL, 0, NULL, 0, NULL);
/*!40000 ALTER TABLE `user_throttle` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
